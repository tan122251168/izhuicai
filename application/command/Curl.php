<?php

namespace app\command;

use app\index\model\DawanjiaModel;
use app\index\model\LotteryDataModel;
use app\index\model\LotteryTypesModel;
use think\console\Command;
use think\console\Input;
use think\console\Output;

class Curl extends Command
{
    protected function configure()
    {
        // 指令配置
        $this->setName('curl');
        // 设置参数
        
    }

    protected function execute(Input $input, Output $output)
    {
        $time = time();
        $dawanjiaModel    = new DawanjiaModel();
        $lotteryDataModel = new LotteryDataModel();
        $lotteryTypesModel = new LotteryTypesModel();

        while(time()<($time+60)){
            $typeList = $lotteryTypesModel->where('parent_id','>','0')->all();
            //遍历全部彩票
            foreach ($typeList as $type){
                $code = $type['code'];
                $lotteryData =  $dawanjiaModel->queryLottery($code );
                if(!isset($lotteryData['data'])||empty($lotteryData['data'])){
                    continue;
                }

                foreach ($lotteryData['data'] as $data){
                    if($lotteryDataModel->findOne(['issue'=>$data['issue'],'lottery_types_id'=>$type['id']])){
                        continue;
                    }
                    $insertData['issue'] = $data['issue'];
                    $insertData['lottery_types_id'] = $type['id'];
                    $insertData['code'] = $code ;
                    $insertData['number'] = implode(',',$data['data']['number']);
                    $insertData['create_time'] = time();
                    $lotteryDataModel->insert( $insertData);
                }
            }
            sleep(3);
        }
        $output->writeln('finish');

    }
}
