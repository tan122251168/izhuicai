<?php

namespace app\command;

use app\common\model\DawanjiaModel;
use app\common\model\LotteryTypeModel;
use app\common\model\OpencodeModel;
use think\console\Command;
use think\console\Input;
use think\console\Output;


class dawanjia extends Command
{
    protected function configure()
    {
        // 指令配置
        $this->setName('dawanjia');
    }

    protected function execute(Input $input, Output $output)
    {

        $time = time();
        $dawanjiaModel    = new DawanjiaModel();
        $opencodeModel = new OpencodeModel();
        $lotteryTypeModel = new LotteryTypeModel();

        while(time()<($time+60)){
            $typeList = $lotteryTypeModel::where('parent_id','>',0)->select();
            //遍历全部彩票
            foreach ($typeList as $type){
                if($type['is_sys']==1){
                    continue;
                }
                $code = $type['code'];
                $lotteryData =  $dawanjiaModel->queryLottery($code );

                if($lotteryData['code']!=200||!isset($lotteryData['data'])||empty($lotteryData['data'])){
                    echo 'data为空忽略：'.$code.PHP_EOL;
                    continue;
                }
                echo $type['id'].'#'.$code;
                foreach ($lotteryData["data"] as $data){
                    if($opencodeModel->findOne(['issue'=>$data['issue'],'lottery_type_id'=>$type['id']])){
                        continue;
                    }
                    $insertData['issue'] = $data['issue'];
                    $insertData['lottery_type_id'] = $type['id'];
                    //特殊格式特殊处理
                    if(in_array($code,['hubk3','gxk3','ahk3','hbk3','gsk3'])){
                        $insertData['opencode'] = implode(',',$data['number']);
                    }else{
                        $insertData['opencode'] = implode(',',$data['data']['number']);
                    }

                    $insertData['update_time'] = $insertData['create_time'] = timeFormat(time());
                    $opencodeModel->insert( $insertData);
                    unset( $insertData);
                }
                unset($lotteryData);
            }
            sleep(3);
        }
        $output->writeln('finish');
    }
}
