<?php

namespace app\command;

use think\console\Command;
use think\console\Input;
use think\console\input\Argument;
use think\console\Output;

/**开奖控制器
 * Class draw
 * @package app\command
 */
class draw extends Command
{
    protected function configure()
    {
        // 指令配置
        $this->setName('draw')
            ->addArgument('type', Argument::OPTIONAL, "type");
        // 设置参数
        
    }

    protected function execute(Input $input, Output $output)
    {
        $type =  trim($input->getArgument('type'));
        switch ($type){
            case 'oneMinute'://1分彩

                break;
            case 'threeMinute'://3分彩

                break;
            case 'fiveMinute'://5分彩

                break;

            case 'tenMinute'://10分彩

                break;
        }
    	// 指令输出
    	$output->writeln('draw');
    }

    /**
     * 1分彩票开奖
     */
    private function oneMinute(){

    }

    /**
     * 3分彩票开奖
     */
    private function threeMinute(){

    }

    /**
     * 5分彩票开奖
     */
    private function fiveMinute(){

    }

    /**
     * 10分彩票开奖
     */
    private function tenMinute(){

    }

}
