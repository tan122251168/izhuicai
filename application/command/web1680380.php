<?php

namespace app\command;

use app\index\model\Www1680380Model;
use think\console\Command;
use think\console\Input;
use think\console\Output;
use app\index\model\LotteryDataModel;
use app\index\model\LotteryTypesModel;

class web1680380 extends Command
{
    protected function configure()
    {
        // 指令配置
        $this->setName('web1680380');
        // 设置参数
        
    }

    protected function execute(Input $input, Output $output)
    {
        $www1680380Model = new Www1680380Model();

        $codeArr = array('bjsc','xyft',
            'cqssc','tjssc','xjssc',
            'jsk3','hbk3','ahk3','nmgk3','fjk3','hubk3','bjk3','gxk3','shk3', 'gzk3','gsk3', 'jlk3',
            'gd11x5','sh11x5','ah11x5','jx11x5','hub11x5','ln11x5','js11x5','zj11x5','nmg11x5');

        foreach ($codeArr as $code){
            $querydata = $www1680380Model->queryLottery($code);

            if(!isset($querydata['errorCode'])||$querydata['errorCode']||!isset($querydata['result'])){
                continue;
            }

            $list = $querydata['result']['data'];
            if(empty($list)){
                continue;
            }

            foreach ($list as $data){
                //数据不完整跳过
                if(!isset($data['preDrawIssue'])||!isset($data['preDrawCode'])){
                    continue;
                }
                $lotteryData['code'] = $code;
                $lotteryData['issue'] = $data['preDrawIssue'];
                $lotteryData['number'] = $data['preDrawCode'];
                $lotteryData['create_time'] = time();
                //数据重复跳过
                $lotteryDataModel = new LotteryDataModel();
                if($lotteryDataModel->findOne(['issue'=>$lotteryData['issue'],'code'=>$code])){
                    continue;
                }
                $lotteryDataModel->save($lotteryData);
                unset($lotteryData);
                unset($data);
            }
        }
    	// 指令输出
    	$output->writeln('web1680380');
    }
}
