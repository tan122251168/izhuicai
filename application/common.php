<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006-2016 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: 流年 <liu21st@gmail.com>
// +----------------------------------------------------------------------

// 应用公共文件

/**生成订单号
 * @param $type业务类型
 * @return int
 */
function createOrdernum($type){
    return date('ymdhis').substr(microtime(),2,4).str_pad(mt_rand(0,99),2,0,STR_PAD_LEFT).str_pad($type,2,0,STR_PAD_LEFT);
}

/**时间格式化
 * @param int $time
 * @return false|string
 */
function timeFormat(int $time){
    return date("Y-m-d H:i:s",$time);
}
