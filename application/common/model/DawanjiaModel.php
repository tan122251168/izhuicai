<?php
/**
 * Created by PhpStorm.
 * User: lenovo
 * Date: 2020/3/19
 * Time: 23:47
 */

namespace app\common\model;


use QL\QueryList;

class DawanjiaModel
{
    private $url = 'http://www.dawanjia.org/api/lottery/results';

    /**抓取彩票数据
     * @param $code
     * @param int $limit
     * @return mixed
     */
    public function queryLottery($code,$limit=3){
        $code = $this->switchCode($code);
        $ql = QueryList::post(
            $this->url,
            [
                'type'=>$code,
                'numbers'=>$limit
            ]
        );
        $html = (string)$ql->getHtml();
        return json_decode( $html ,true);
    }

    /**
    'hljssc','hbssc','jlssc','nmgssc','tjssc','xjssc','ynssc','cqssc',
    'xglhc',
    'bjpk10','xyft',
    'gdklsf','gxklsf','hljklsf','hnklsf','shxklsf','sxklsf', 'tjklsf','ynklsf','cqklsf',
    'fjk3','gsk3','gxk3','gzk3','hbk3','hnk3','hubk3','jlk3','jsk3','jxk3','nmk3','nxk3','qhk3','shk3','xzk3',
    'ahsyxw','bjsyxw','fjsyxw','gssyxw','gdsyxw','gxsyxw','gzsyxw','hebsyxw','hljsyxw','hbsyxw','jlsyxw',
    'jssyxw','jxsyxw','lnsyxw','nmgsyxw','nxsyxw','qhsyxw','sdsyxw','shxsyxw','sxsyxw','shsyxw','tjsyxw',
    'xzsyxw','ynsyxw','zjsyxw',
    'bjklb','jndklb','jpzklb',
    'bjxy28'
     */

    /**
     * 转换彩票代码
     */
    public function switchCode($code){
        $typeList = array(
            'hljssc'=>'hljssc','hbssc'=>'hbssc','jlssc'=>'jlssc','nmgssc'=>'nmgssc','tjssc'=>'tjssc','xjssc'=>'xjssc','ynssc'=>'ynssc','cqssc'=>'cqssc',
            'xglhc'=>'xglhc',
            'bjpk10'=>'bjsc','xyft'=>'xyft',
            'gdklsf'=>'gdklsf','gxklsf'=>'gxklsf','hljklsf'=>'hljklsf','hnklsf'=>'hnklsf','shxklsf'=>'shxklsf','sxklsf'=>'sxklsf',
            'tjklsf'=>'tjklsf','ynklsf'=>'ynklsf','cqklsf'=>'cqklsf',
            'fjk3'=>'fjk3','gsk3'=>'gsk3','gxk3'=>'gxk3','gzk3'=>'gzk3','hbk3'=>'hbk3',
            'hnk3'=>'hnk3','hubk3'=>'hubk3','jlk3'=>'jlk3','jsk3'=>'jsk3','jxk3'=>'jxk3','ahk3'=>'ahk3',
            'nmk3'=>'nmk3','nxk3'=>'nxk3','qhk3'=>'qhk3','shk3'=>'shk3','xzk3'=>'xzk3','ahks'=>'ahks',
            'ahsyxw'=>'ahsyxw','bjsyxw'=>'bjsyxw','fjsyxw'=>'fjsyxw','gssyxw'=>'gssyxw',
            'gdsyxw'=>'gdsyxw','gxsyxw'=>'gxsyxw','gzsyxw'=>'gzsyxw','hebsyxw'=>'hebsyxw','hljsyxw'=>'hljsyxw','hbsyxw'=>'hbsyxw','jlsyxw'=>'jlsyxw',
            'jssyxw'=>'jssyxw','jxsyxw'=>'jxsyxw','lnsyxw'=>'lnsyxw','nmgsyxw'=>'nmgsyxw','nxsyxw'=>'nxsyxw',
            'qhsyxw'=>'qhsyxw','sdsyxw'=>'sdsyxw','shxsyxw'=>'shxsyxw','sxsyxw'=>'sxsyxw','shsyxw'=>'shsyxw','tjsyxw'=>'tjsyxw',
            'xzsyxw'=>'xzsyxw','ynsyxw'=>'ynsyxw','zjsyxw'=>'zjsyxw',
            'bjklb'=>'bjklb','jndklb'=>'jndklb','jpzklb'=>'jpzklb',
            'bjxy28'=>'bjxy28'
        );
        return $typeList[$code];
    }
}