<?php
/**
 * Created by PhpStorm.
 * User: lenovo
 * Date: 2020/3/20
 * Time: 0:11
 */

namespace app\common\model;


use think\Model;

class LotteryTypeModel extends Model
{
    /**
     * 父级列表
     */
    public function findByParentId($parentId){
        $list = $this->where(['parent_id'=>$parentId])->order('id', 'asc')->select();
        return $list ;
    }


    /**
     * 数据表名称
     * @var string
     */
    protected $table='lottery_type';

    /**获取下期期号、时间信息
     * @param $code
     */
    public function nextIssue($code){
         switch ($code){
             //一分时时彩
             case 'yfssc':
                 $start     = strtotime(date("Y-m-d").' 00:00:00');//开始销售时间
                 $end       = strtotime(date("Y-m-d").' 24:00:00');//最后一期开奖时间
                 $steps     = 60*1;
                 $time = time();
                 $round = floor(($time-$start)/$steps);
                 //下期
                 $issue    = date("Ymd"). str_pad(0,4,$round+1);
                 $opentime = $start+($round*$steps)+$steps;
                 $currenIssue = date("Ymd"). str_pad(0,4,$round);
                 break;
             //三分时时彩
             case 'sfssc':
                 $start     = strtotime(date("Y-m-d").' 00:00:00');//开始销售时间
                 $end       = strtotime(date("Y-m-d").' 24:00:00');//最后一期开奖时间
                 $steps     = 60*3;
                 $time = time();
                 $round = floor(($time-$start)/$steps);
                 //下期
                 $issue = date("Ymd").str_pad($round+1,3,'0',STR_PAD_LEFT);
                 $opentime = $start+($round*$steps)+$steps;
                break;
             //五分时时彩
             case 'wfssc':
                 $start     = strtotime(date("Y-m-d").' 00:00:00');//开始销售时间
                 $end       = strtotime(date("Y-m-d").' 24:00:00');//最后一期开奖时间
                 $steps     = 60*5;
                 $time = time();
                 $round = floor(($time-$start)/$steps);
                 //下期
                 $issue = date("Ymd").str_pad($round+1,3,'0',STR_PAD_LEFT);
                 $opentime = $start+($round*$steps)+$steps;
                 break;
                  //重庆时时彩20200320059
             case 'cqssc':
                $start     = strtotime(date("Y-m-d").' 00:10:00');//开始销售时间
                $end       = strtotime(date("Y-m-d").' 23:50:00');//最后一期开奖时间
                $break     = strtotime(date("Y-m-d").' 03:10:00');//中停止时间
                $restart   = strtotime(date("Y-m-d").' 07:10:00');//重新开盘时间，7点30开奖
                $steps     = 60*20;
                $time = time();
                //最新几期，00:10:100 - 3:10:10
                if($time<$break){
                        //前一天最后一期
                        if($time<$start){
                            $opentime = $start-$time;
                            $issue    = date("Ymd",$time-86400).'059';
                        }else{
                            $round = ($time-$start)/$steps;
                            $issue    = date("Ymd"). str_pad(0,3,$round);
                            $opentime = $start+($round*$steps)+$steps;
                        }
                }elseif($time>=$end){//已经过了最后一期,第二天第一期
                        $opentime = ($start+86400+$steps)-$time;
                        $issue    = date("Ymd",$time+86400).'001';
                }else{//07:10:00-23:50:00
                        $startRound= 9;//第九期暂停
                        //中断期间
                        if($time<=$restart){
                            $opentime = $restart+$steps;
                            $issue    = date("Ymd",$time+86400).'010';
                        }else{
                            $round = floor(($time-$restart)/$steps);
                            //下期
                            $issue    = date("Ymd"). str_pad(0,3,$round+$startRound+1);
                            $opentime = $restart+($round*$steps)+$steps;
                        }
                }
                break;
                 //天津时时彩，每天42期
             case 'tjssc':
                 $start     = strtotime(date("Y-m-d").' 09:00:00');//开始销售时间
                 $end       = strtotime(date("Y-m-d").' 23:00:00');//最后一期开奖时间
                 $steps     = 60*20;
                 $time = time();
                 //未到第一期
                 if($time<$start){
                     $opentime = $start-$time;
                     $issue    = date("Ymd",$time).'001';
                 }elseif($time>=$end){
                     $opentime = $start+86400;
                     $issue    = date("Ymd",$time+86400).'001';
                 }else{
                     $round = floor(($time-$start)/$steps);
                     $issue    = date("Ymd"). str_pad(0,3,$round+1);
                     $opentime = $start+($round*$steps)+$steps;
                 }
                 break;
              case 'xjssc'://新疆时时彩
                  $start     = strtotime(date("Y-m-d").' 10:00:00');//开始销售时间
                  $end       = strtotime(date("Y-m-d").' 02:00:00')+86400;//最后一期开奖时间
                  $steps     = 60*20;
                  $time = time();
                  if($time>=$start){
                      $round = floor(($time-$start)/$steps);
                      $issue    = date("Ymd",$start). str_pad(0,3,$round+1);
                      $opentime = $start+($round*$steps)+$steps;
                  }else{//半夜封盘到早上开盘
                      $opentime = $start+$steps;
                      $round = 0;
                      $issue = date("Ymd",$start). str_pad(0,3,$round+1);
                  }
                  break;
             case 'hljssc'://黑龙江时时彩,期号有错，等修复
                 $start     = strtotime(date("Y-m-d").' 08:40:00');//开始销售时间
                 $end       = strtotime(date("Y-m-d").' 22:40:00');//最后一期开奖时间
                 $steps     = 60*20;
                 $time = time();
                 //未到第一期
                 if($time<$start){
                     $opentime = $start-$time;
                     $issue    = date("Ymd",$time).'001';
                 }elseif($time>=$end){
                     $opentime = $start+86400;
                     $issue    = date("Ymd",$time+86400).'001';
                 }else{
                     $round = floor(($time-$start)/$steps);
                     $issue    = date("Ymd"). str_pad(0,3,$round+1);
                     $opentime = $start+($round*$steps)+$steps;
                 }
                 break;
             case 'hbssc'://湖北时时彩
                 $total = 39;
                 $start     = strtotime(date("Y-m-d").' 08:50:00');//开始销售时间
                 $end       = strtotime(date("Y-m-d").' 21:50:00');//最后一期开奖时间
                 $steps     = 60*20;
                 $time = time();
                 //未到第一期
                 if($time<$start){
                     $opentime = $start-$time;
                     $issue    = date("Ymd",$time).'001';
                 }elseif($time>=$end){
                     $opentime = $start+86400;
                     $issue    = date("Ymd",$time+86400).'001';
                 }else{
                     $round = floor(($time-$start)/$steps);
                     $issue    = date("Ymd"). str_pad($round+1,3,'0',STR_PAD_LEFT);
                     $opentime = $start+($round*$steps)+$steps;
                 }

                break;

             case 'jlssc'://吉林时时彩,会提前开奖
                 $total = 40;
                 $start     = strtotime(date("Y-m-d").' 08:10:00');//开始销售时间
                 $end       = strtotime(date("Y-m-d").' 21:30:00');//最后一期开奖时间
                 $steps     = 60*20;
                 $time = time();
                 //未到第一期
                 if($time<$start){
                     $opentime = $start-$time;
                     $issue    = date("Ymd",$time).'001';
                 }elseif($time>=$end){
                     $opentime = $start+86400;
                     $issue    = date("Ymd",$time+86400).'001';
                 }else{
                     $round = floor(($time-$start)/$steps);
                     $issue    = date("Ymd"). str_pad($round+1,3,'0',STR_PAD_LEFT);
                     $opentime = $start+($round*$steps)+$steps;
                 }
                break;

             case 'nmgssc'://内蒙古时时彩20200323001
                 $start     = strtotime(date("Y-m-d").' 09:30:00');//开始销售时间
                 $end       = strtotime(date("Y-m-d").' 21:30:00');//最后一期开奖时间
                 $steps     = 60*20;
                 $time = time();
                 //未到第一期
                 if($time<$start){
                     $opentime = $start-$time;
                     $issue    = date("Ymd",$time).'001';
                 }elseif($time>=$end){
                     $opentime = $start+86400;
                     $issue    = date("Ymd",$time+86400).'001';
                 }else{
                     $round = floor(($time-$start)/$steps);
                     $issue    = date("Ymd"). str_pad(0,3,$round+1);
                     $opentime = $start+($round*$steps)+$steps;
                 }
                 break;
             case 'ynssc'://云南时时彩
                 $total = 36;
                 $start     = strtotime(date("Y-m-d").' 09:40:00');//开始销售时间
                 $end       = strtotime(date("Y-m-d").' 21:40:00');//最后一期开奖时间
                 $steps     = 60*20;
                 $time = time();
                 //未到第一期
                 if($time<$start){
                     $opentime = $start-$time;
                     $issue    = date("Ymd",$time).'001';
                 }elseif($time>=$end){
                     $opentime = $start+86400;
                     $issue    = date("Ymd",$time+86400).'001';
                 }else{
                     $round = floor(($time-$start)/$steps);
                     $issue    = date("Ymd"). str_pad(0,3,$round+1);
                     $opentime = $start+($round*$steps)+$steps;
                 }
                 break;

             //一分PK10
             case 'yfpk10':
                 $start     = strtotime(date("Y-m-d").' 00:00:00');//开始销售时间
                 $end       = strtotime(date("Y-m-d").' 24:00:00');//最后一期开奖时间
                 $steps     = 60*1;
                 $time = time();
                 $round = floor(($time-$start)/$steps);
                 //下期
                 $issue = date("Ymd").str_pad($round+1,3,'0',STR_PAD_LEFT);
                 $opentime = $start+($round*$steps)+$steps;

                 break;
             //三分PK10
             case 'sfpk10':
                 $start     = strtotime(date("Y-m-d").' 00:00:00');//开始销售时间
                 $end       = strtotime(date("Y-m-d").' 24:00:00');//最后一期开奖时间
                 $steps     = 60*3;
                 $time = time();
                 $round = floor(($time-$start)/$steps);
                 //下期
                 $issue = date("Ymd").str_pad($round+1,3,'0',STR_PAD_LEFT);
                 $opentime = $start+($round*$steps)+$steps;
                 break;
             //五分PK10
             case 'wfpk10':
                 $start     = strtotime(date("Y-m-d").' 00:00:00');//开始销售时间
                 $end       = strtotime(date("Y-m-d").' 24:00:00');//最后一期开奖时间
                 $steps     = 60*5;
                 $time = time();
                 $round = floor(($time-$start)/$steps);
                 //下期
                 $issue =str_pad($round+1,3,'0',STR_PAD_LEFT);
                 $opentime = $start+($round*$steps)+$steps;
                 break;

             case 'xyft'://幸运飞艇、马其他飞艇,期号都是用累加的
                 $total = 180;
                 $start     = strtotime(date("Y-m-d").' 13:04:00');//开始销售时间
                 $end       = strtotime(date("Y-m-d").' 04:05:00')+86400;//最后一期开奖时间
                 $steps     = 60*5;
                 $time = time();
                 //开盘之前
                 if($time<$start){
                     //上一天的
                     if($time<($end-86400)){
                         $round = floor(($time-$start-86400)/$steps);
                         $issue    = $round+1;
                         $opentime = $start+($round*$steps)+$steps;
                     }else{//今天未开盘
                         $issue    = 1;
                         $opentime = $start+$steps;
                     }
                 }else{
                     $round = floor(($time-$start)/$steps);
                     $issue    = $round+1;
                     $opentime = $start+($round*$steps)+$steps;
                 }
                 $remainIssue = $total-$round;
                 break;
             case 'bjpk10'://PK10，期号都是用累加的，注意没开的情况
                 $start     = strtotime(date("Y-m-d").' 09:30:00');//开始销售时间
                 $end       = strtotime(date("Y-m-d").' 21:30:00');//最后一期开奖时间
                 $steps     = 60*20;
                 $time = time();
                break;


             //一分11选5
             case 'yf11x5':
                 $start     = strtotime(date("Y-m-d").' 00:00:00');//开始销售时间
                 $end       = strtotime(date("Y-m-d").' 24:00:00');//最后一期开奖时间
                 $steps     = 60*1;
                 $time = time();
                 $round = floor(($time-$start)/$steps);
                 //下期
                 $issue    = date("Ymd"). str_pad(0,4,$round+1);
                 $opentime = $start+($round*$steps)+$steps;

                 break;
             //三分11选5
             case 'sf11x5':
                 $start     = strtotime(date("Y-m-d").' 00:00:00');//开始销售时间
                 $end       = strtotime(date("Y-m-d").' 24:00:00');//最后一期开奖时间
                 $steps     = 60*3;
                 $time = time();
                 $round = floor(($time-$start)/$steps);
                 //下期
                 $issue = date("Ymd").str_pad($round+1,3,'0',STR_PAD_LEFT);
                 $opentime = $start+($round*$steps)+$steps;
                 break;
             //五分11选5
             case 'wf11x5':
                 $start     = strtotime(date("Y-m-d").' 00:00:00');//开始销售时间
                 $end       = strtotime(date("Y-m-d").' 24:00:00');//最后一期开奖时间
                 $steps     = 60*5;
                 $time = time();
                 $round = floor(($time-$start)/$steps);
                 //下期
                 $issue = date("Ymd").str_pad($round+1,3,'0',STR_PAD_LEFT);
                 $opentime = $start+($round*$steps)+$steps;
                 break;

             case 'sd11x5'://山东11选5
                 $total = 43;
                 $start     = strtotime(date("Y-m-d").' 08:40:00');//开始销售时间
                 $end       = strtotime(date("Y-m-d").' 23:00:00');//最后一期开奖时间
                 $steps     = 60*20;
                 $time = time();
                 //未到第一期
                 if($time<$start){
                     $opentime = $start-$time;
                     $issue    = date("Ymd",$time).'001';
                 }elseif($time>=$end){
                     $opentime = $start+86400;
                     $issue    = date("Ymd",$time+86400).'001';
                 }else{
                     $round = floor(($time-$start)/$steps);
                     $issue    = date("Ymd"). str_pad(0,3,$round+1);
                     $opentime = $start+($round*$steps)+$steps;
                 }
                 break;

             case 'gd11x5'://广东11选5
                 $total = 42;
                 $start     = strtotime(date("Y-m-d").' 09:10:00');//开始销售时间
                 $end       = strtotime(date("Y-m-d").' 23:10:00');//最后一期开奖时间
                 $steps     = 60*20;
                 $time = time();
                 //未到第一期
                 if($time<$start){
                     $opentime = $start-$time;
                     $issue    = date("Ymd",$time).'001';
                 }elseif($time>=$end){
                     $opentime = $start+86400;
                     $issue    = date("Ymd",$time+86400).'001';
                 }else{
                     $round = floor(($time-$start)/$steps);
                     $issue    = date("Ymd"). str_pad(0,3,$round+1);
                     $opentime = $start+($round*$steps)+$steps;
                 }
                 break;

             case 'jx11x5'://江西11选5
                 $total = 42;
                 $start     = strtotime(date("Y-m-d").' 09:10:00');//开始销售时间
                 $end       = strtotime(date("Y-m-d").' 23:10:00');//最后一期开奖时间
                 $steps     = 60*20;
                 $time = time();
                 //未到第一期
                 if($time<$start){
                     $opentime = $start-$time;
                     $issue    = date("Ymd",$time).'001';
                 }elseif($time>=$end){
                     $opentime = $start+86400;
                     $issue    = date("Ymd",$time+86400).'001';
                 }else{
                     $round = floor(($time-$start)/$steps);
                     $issue    = date("Ymd"). str_pad(0,3,$round+1);
                     $opentime = $start+($round*$steps)+$steps;
                 }
                 break;

             case 'sh11x5'://上海11选5
                 $total = 45;
                 $start     = strtotime(date("Y-m-d").' 09:00:00');//开始销售时间
                 $end       = strtotime(date("Y-m-d").' 23:40:00');//最后一期开奖时间
                 $steps     = 60*20;
                 $time = time();
                 //未到第一期
                 if($time<$start){
                     $opentime = $start-$time;
                     $issue    = date("Ymd",$time).'01';
                 }elseif($time>=$end){
                     $opentime = $start+86400;
                     $issue    = date("Ymd",$time+86400).'01';
                 }else{
                     $round = floor(($time-$start)/$steps);
                      if($round<9){
                          $issue    = date("Ymd"). str_pad(0,2,$round+1);
                      }else{
                          $issue    = date("Ymd"). ($round+1);
                      }
                     $opentime = $start+($round*$steps)+$steps;
                 }
                 break;
             case 'ah11x5'://安徽11选5
                 $total = 40;
                 $start     = strtotime(date("Y-m-d").' 08:40:00');//开始销售时间
                 $end       = strtotime(date("Y-m-d").' 22:00:00');//最后一期开奖时间
                 $steps     = 60*20;
                 $time = time();
                 //未到第一期
                 if($time<$start){
                     $opentime = $start-$time;
                     $issue    = date("Ymd",$time).'01';
                 }elseif($time>=$end){
                     $opentime = $start+86400;
                     $issue    = date("Ymd",$time+86400).'01';
                 }else{
                     $round = floor(($time-$start)/$steps);
                     if($round<9){
                         $issue    = date("Ymd"). str_pad(0,2,$round+1);
                     }else{
                         $issue    = date("Ymd"). ($round+1);
                     }
                     $opentime = $start+($round*$steps)+$steps;
                 }
                 break;
             case 'bj11x5'://北京11选5
                 $total = 42;
                 $start     = strtotime(date("Y-m-d").' 09:00:00');//开始销售时间
                 $end       = strtotime(date("Y-m-d").' 23:00:00');//最后一期开奖时间
                 $steps     = 60*20;
                 $time = time();
                 //未到第一期
                 if($time<$start){
                     $opentime = $start-$time;
                     $issue    = date("Ymd",$time).'01';
                 }elseif($time>=$end){
                     $opentime = $start+86400;
                     $issue    = date("Ymd",$time+86400).'01';
                 }else{
                     $round = floor(($time-$start)/$steps);
                     if($round<9){
                         $issue    = date("Ymd"). str_pad(0,2,$round+1);
                     }else{
                         $issue    = date("Ymd"). ($round+1);
                     }
                     $opentime = $start+($round*$steps)+$steps;
                 }
                 break;
             case 'fj11x5'://福建11选5
                 $total = 45;
                 $start     = strtotime(date("Y-m-d").' 08:10:00');//开始销售时间
                 $end       = strtotime(date("Y-m-d").' 23:10:00');//最后一期开奖时间
                 $steps     = 60*20;
                 $time = time();
                 //未到第一期
                 if($time<$start){
                     $opentime = $start-$time;
                     $issue    = date("Ymd",$time).'01';
                 }elseif($time>=$end){
                     $opentime = $start+86400;
                     $issue    = date("Ymd",$time+86400).'01';
                 }else{
                     $round = floor(($time-$start)/$steps);
                     if($round<9){
                         $issue    = date("Ymd"). str_pad(0,2,$round+1);
                     }else{
                         $issue    = date("Ymd"). ($round+1);
                     }
                     $opentime = $start+($round*$steps)+$steps;
                 }
                 break;
             case 'gs11x5'://甘肃11选5
                 $total = 39;
                 $start     = strtotime(date("Y-m-d").' 10:10:00');//开始销售时间
                 $end       = strtotime(date("Y-m-d").' 23:10:00');//最后一期开奖时间
                 $steps     = 60*20;
                 $time = time();
                 //未到第一期
                 if($time<$start){
                     $opentime = $start-$time;
                     $issue    = date("Ymd",$time).'01';
                 }elseif($time>=$end){
                     $opentime = $start+86400;
                     $issue    = date("Ymd",$time+86400).'01';
                 }else{
                     $round = floor(($time-$start)/$steps);
                     if($round<9){
                         $issue    = date("Ymd"). str_pad(0,2,$round+1);
                     }else{
                         $issue    = date("Ymd"). ($round+1);
                     }
                     $opentime = $start+($round*$steps)+$steps;
                 }
                 break;
             case 'gz11x5'://贵州11选5
                 $total = 40;
                 $start     = strtotime(date("Y-m-d").' 09:00:00');//开始销售时间
                 $end       = strtotime(date("Y-m-d").' 22:20:00');//最后一期开奖时间
                 $steps     = 60*20;
                 $time = time();
                 //未到第一期
                 if($time<$start){
                     $opentime = $start-$time;
                     $issue    = date("Ymd",$time).'01';
                 }elseif($time>=$end){
                     $opentime = $start+86400;
                     $issue    = date("Ymd",$time+86400).'01';
                 }else{
                     $round = floor(($time-$start)/$steps);
                     if($round<9){
                         $issue    = date("Ymd"). str_pad(0,2,$round+1);
                     }else{
                         $issue    = date("Ymd"). ($round+1);
                     }
                     $opentime = $start+($round*$steps)+$steps;
                 }
                 break;
             case 'heb11x5'://河北11选5
                 $total = 42;
                 $start     = strtotime(date("Y-m-d").' 08:30:00');//开始销售时间
                 $end       = strtotime(date("Y-m-d").' 22:30:00');//最后一期开奖时间
                 $steps     = 60*20;
                 $time = time();
                 //未到第一期
                 if($time<$start){
                     $opentime = $start-$time;
                     $issue    = date("Ymd",$time).'01';
                 }elseif($time>=$end){
                     $opentime = $start+86400;
                     $issue    = date("Ymd",$time+86400).'01';
                 }else{
                     $round = floor(($time-$start)/$steps);
                     if($round<9){
                         $issue    = date("Ymd"). str_pad(0,2,$round+1);
                     }else{
                         $issue    = date("Ymd"). ($round+1);
                     }
                     $opentime = $start+($round*$steps)+$steps;
                 }
                 break;
             case 'hlj11x5'://黑龙江11选5
                 $total = 44;
                 $start     = strtotime(date("Y-m-d").' 08:05:00');//开始销售时间
                 $end       = strtotime(date("Y-m-d").' 22:30:00');//最后一期开奖时间
                 $steps     = 60*20;
                 $time = time();
                 //未到第一期
                 if($time<$start){
                     $opentime = $start-$time;
                     $issue    = date("Ymd",$time).'01';
                 }elseif($time>=$end){
                     $opentime = $start+86400;
                     $issue    = date("Ymd",$time+86400).'01';
                 }else{
                     $round = floor(($time-$start)/$steps);
                     if($round<9){
                         $issue    = date("Ymd"). str_pad(0,2,$round+1);
                     }else{
                         $issue    = date("Ymd"). ($round+1);
                     }
                     $opentime = $start+($round*$steps)+$steps;
                 }
                 break;
             case 'hubs11x5'://湖北11选5
                 $total = 40;
                 $start     = strtotime(date("Y-m-d").' 08:35:00');//开始销售时间
                 $end       = strtotime(date("Y-m-d").' 21:55:00');//最后一期开奖时间
                 $steps     = 60*20;
                 $time = time();
                 //未到第一期
                 if($time<$start){
                     $opentime = $start-$time;
                     $issue    = date("Ymd",$time).'01';
                 }elseif($time>=$end){
                     $opentime = $start+86400;
                     $issue    = date("Ymd",$time+86400).'01';
                 }else{
                     $round = floor(($time-$start)/$steps);
                     if($round<9){
                         $issue    = date("Ymd"). str_pad(0,2,$round+1);
                     }else{
                         $issue    = date("Ymd"). ($round+1);
                     }
                     $opentime = $start+($round*$steps)+$steps;
                 }
                 break;
             case 'js11x5'://江苏11选5
                 $total = 41;
                 $start     = strtotime(date("Y-m-d").' 08:25:00');//开始销售时间
                 $end       = strtotime(date("Y-m-d").' 22:05:00');//最后一期开奖时间
                 $steps     = 60*20;
                 $time = time();
                 //未到第一期
                 if($time<$start){
                     $opentime = $start-$time;
                     $issue    = date("Ymd",$time).'01';
                 }elseif($time>=$end){
                     $opentime = $start+86400;
                     $issue    = date("Ymd",$time+86400).'01';
                 }else{
                     $round = floor(($time-$start)/$steps);
                     if($round<9){
                         $issue    = date("Ymd"). str_pad(0,2,$round+1);
                     }else{
                         $issue    = date("Ymd"). ($round+1);
                     }
                     $opentime = $start+($round*$steps)+$steps;
                 }
                 break;
             case 'jl11x5'://吉林11选5
                 $total = 39;
                 $start     = strtotime(date("Y-m-d").' 08:30:00');//开始销售时间
                 $end       = strtotime(date("Y-m-d").' 21:30:00');//最后一期开奖时间
                 $steps     = 60*20;
                 $time = time();
                 //未到第一期
                 if($time<$start){
                     $opentime = $start-$time;
                     $issue    = date("Ymd",$time).'01';
                 }elseif($time>=$end){
                     $opentime = $start+86400;
                     $issue    = date("Ymd",$time+86400).'01';
                 }else{
                     $round = floor(($time-$start)/$steps);
                     if($round<9){
                         $issue    = date("Ymd"). str_pad(0,2,$round+1);
                     }else{
                         $issue    = date("Ymd"). ($round+1);
                     }
                     $opentime = $start+($round*$steps)+$steps;
                 }
                 break;
             case 'ln11x5'://辽宁11选5
                 $total = 41;
                 $start     = strtotime(date("Y-m-d").' 08:50:00');//开始销售时间
                 $end       = strtotime(date("Y-m-d").' 22:30:00');//最后一期开奖时间
                 $steps     = 60*20;
                 $time = time();
                 //未到第一期
                 if($time<$start){
                     $opentime = $start-$time;
                     $issue    = date("Ymd",$time).'01';
                 }elseif($time>=$end){
                     $opentime = $start+86400;
                     $issue    = date("Ymd",$time+86400).'01';
                 }else{
                     $round = floor(($time-$start)/$steps);
                     if($round<9){
                         $issue    = date("Ymd"). str_pad(0,2,$round+1);
                     }else{
                         $issue    = date("Ymd"). ($round+1);
                     }
                     $opentime = $start+($round*$steps)+$steps;
                 }
                 break;

             case 'nmg11x5'://内蒙古11选5
                 $total = 42;
                 $start     = strtotime(date("Y-m-d").' 09:05:00');//开始销售时间
                 $end       = strtotime(date("Y-m-d").' 23:05:00');//最后一期开奖时间
                 $steps     = 60*20;
                 $time = time();
                 //未到第一期
                 if($time<$start){
                     $opentime = $start-$time;
                     $issue    = date("Ymd",$time).'01';
                 }elseif($time>=$end){
                     $opentime = $start+86400;
                     $issue    = date("Ymd",$time+86400).'01';
                 }else{
                     $round = floor(($time-$start)/$steps);
                     if($round<9){
                         $issue    = date("Ymd"). str_pad(0,2,$round+1);
                     }else{
                         $issue    = date("Ymd"). ($round+1);
                     }
                     $opentime = $start+($round*$steps)+$steps;
                 }
                 break;
             case 'nx11x5'://宁夏11选5
                 $total = 38;
                 $start     = strtotime(date("Y-m-d").' 09:05:00');//开始销售时间
                 $end       = strtotime(date("Y-m-d").' 21:45:00');//最后一期开奖时间
                 $steps     = 60*20;
                 $time = time();
                 //未到第一期
                 if($time<$start){
                     $opentime = $start-$time;
                     $issue    = date("Ymd",$time).'01';
                 }elseif($time>=$end){
                     $opentime = $start+86400;
                     $issue    = date("Ymd",$time+86400).'01';
                 }else{
                     $round = floor(($time-$start)/$steps);
                     if($round<9){
                         $issue    = date("Ymd"). str_pad(0,2,$round+1);
                     }else{
                         $issue    = date("Ymd"). ($round+1);
                     }
                     $opentime = $start+($round*$steps)+$steps;
                 }
                 break;

             case 'qh11x5'://青海11选5
                 $total = 41;
                 $start     = strtotime(date("Y-m-d").' 09:15:00');//开始销售时间
                 $end       = strtotime(date("Y-m-d").' 22:55:00');//最后一期开奖时间
                 $steps     = 60*20;
                 $time = time();
                 //未到第一期
                 if($time<$start){
                     $opentime = $start-$time;
                     $issue    = date("Ymd",$time).'01';
                 }elseif($time>=$end){
                     $opentime = $start+86400;
                     $issue    = date("Ymd",$time+86400).'01';
                 }else{
                     $round = floor(($time-$start)/$steps);
                     if($round<9){
                         $issue    = date("Ymd"). str_pad(0,2,$round+1);
                     }else{
                         $issue    = date("Ymd"). ($round+1);
                     }
                     $opentime = $start+($round*$steps)+$steps;
                 }
                 break;

             case 'shx11x5'://山西11选5
                 $total = 47;
                 $start     = strtotime(date("Y-m-d").' 08:25:00');//开始销售时间
                 $end       = strtotime(date("Y-m-d").' 00:05:00')+86400;//最后一期开奖时间
                 $steps     = 60*20;
                 $time = time();
                 //未到第一期
                 if($time<$start){
                     //凌晨，前一期最后一期
                     if($time<($end-86400)){
                         $opentime = $end-86400;
                         $issue    = date("Ymd",$time-86400).'47';
                     }else{
                         $opentime = $start-$time;
                         $issue    = date("Ymd",$time).'01';
                     }
                 }else{
                     $round = floor(($time-$start)/$steps);
                     $issue    = date("Ymd"). ($round+1);
                     $opentime = $start+($round*$steps)+$steps;
                 }
                 break;
             case 'sxx11x5'://陕西11选5
                 $total = 44;
                 $start     = strtotime(date("Y-m-d").' 08:30:00');//开始销售时间
                 $end       = strtotime(date("Y-m-d").' 23:10:00');//最后一期开奖时间
                 $steps     = 60*20;
                 $time = time();
                 //未到第一期
                 if($time<$start){
                     $opentime = $start-$time;
                     $issue    = date("Ymd",$time).'01';
                 }elseif($time>=$end){//结束到第二天
                     $opentime = $start+86400-$time;
                     $issue    = date("Ymd",$time).'01';
                 }else{
                     $round = floor(($time-$start)/$steps);
                     $issue    = date("Ymd"). ($round+1);//因为是第一期开始的，不是0
                     $opentime = $start+($round*$steps)+$steps;
                 }
                 break;
             case 'tj11x5'://天津11选5
                 $total = 45;
                 $start     = strtotime(date("Y-m-d").' 09:00:00');//开始销售时间
                 $end       = strtotime(date("Y-m-d").' 00:00:00')+86400;//最后一期开奖时间
                 $steps     = 60*20;
                 $time = time();
                 //未到第一期
                 if($time<$start){
                     $opentime = $start-$time;
                     $issue    = date("Ymd",$time).'01';
                 }else{
                     $round = floor(($time-$start)/$steps);
                     $issue    = date("Ymd"). ($round+1);//因为是第一期开始的，不是0
                     $opentime = $start+($round*$steps)+$steps;
                 }
                 break;
             case 'xz11x5'://西藏11选5
                 $total = 42;
                 $start     = strtotime(date("Y-m-d").' 09:10:00');//开始销售时间
                 $end       = strtotime(date("Y-m-d").' 23:10:00');//最后一期开奖时间
                 $steps     = 60*20;
                 $time = time();
                 //未到第一期
                 if($time<$start){
                     $opentime = $start+$steps;
                     $issue    = date("Ymd",$time).'01';
                 }elseif($time>=$end){
                     $opentime = $start+$steps+86400;
                     $issue    = date("Ymd",$time+86400).'01';
                 }else{
                     $round = floor(($time-$start)/$steps);
                     $issue    = date("Ymd"). ($round+1);//因为是第一期开始的，不是0
                     $opentime = $start+($round*$steps)+$steps;
                 }
                 break;
             case 'xj11x5'://新疆11选5
                 $total = 48;
                 $start     = strtotime(date("Y-m-d").' 10:00:00');//开始销售时间
                 $end       = strtotime(date("Y-m-d").' 02:00:00')+time();//最后一期开奖时间
                 $steps     = 60*20;
                 $time = time();
                 //未到第一期
                 if($time<=$start){
                     //前一天的还没结束
                     if($time<=($end-86400)){
                         $start = $start-86400;
                         $round = floor(($time-$start)/$steps);
                         $issue    = date("Ymd",$start). ($round+1);//因为是第一期开始的，不是0
                         $opentime = $start+($round*$steps)+$steps;
                     }else{
                         $opentime = $start+$steps;
                         $issue    = date("Ymd",$time).'01';
                     }
                 }else{
                     $round = floor(($time-$start)/$steps);
                     $issue    = date("Ymd"). ($round+1);//因为是第一期开始的，不是0
                     $opentime = $start+($round*$steps)+$steps;
                 }
                 break;
             case 'yn11x5'://云南11选5
                 $total = 42;
                 $start     = strtotime(date("Y-m-d").' 09:00:00');//开始销售时间
                 $end       = strtotime(date("Y-m-d").' 23:10:00');//最后一期开奖时间
                 $steps     = 60*20;
                 $time = time();
                 //未到第一期
                 if($time<$start){
                     $opentime = $start+$steps;
                     $issue    = date("Ymd",$time).'01';
                 }elseif($time>=$end){
                     $opentime = $start+$steps+86400;
                     $issue    = date("Ymd",$time+86400).'01';
                 }else{
                     $round = floor(($time-$start)/$steps);
                     $issue    = date("Ymd"). ($round+1);//因为是第一期开始的，不是0
                     $opentime = $start+($round*$steps)+$steps;
                 }
                 break;

             case 'zj11x5'://浙江11选5
                 $total = 42;
                 $start     = strtotime(date("Y-m-d").' 08:30:00');//开始销售时间
                 $end       = strtotime(date("Y-m-d").' 22:30:00');//最后一期开奖时间
                 $steps     = 60*20;
                 $time = time();
                 //未到第一期
                 if($time<$start){
                     $opentime = $start+$steps;
                     $issue    = date("Ymd",$time).'01';
                 }elseif($time>=$end){
                     $opentime = $start+$steps+86400;
                     $issue    = date("Ymd",$time+86400).'01';
                 }else{
                     $round = floor(($time-$start)/$steps);
                     $issue    = date("Ymd"). ($round+1);//因为是第一期开始的，不是0
                     $opentime = $start+($round*$steps)+$steps;
                 }
                 break;


             //一分快3
             case 'yfk3':
                 $start     = strtotime(date("Y-m-d").' 00:00:00');//开始销售时间
                 $end       = strtotime(date("Y-m-d").' 24:00:00');//最后一期开奖时间
                 $steps     = 60*1;
                 $time = time();
                 $round = floor(($time-$start)/$steps);
                 //下期
                 $issue    = date("Ymd"). str_pad(0,4,$round+1);
                 $opentime = $start+($round*$steps)+$steps;

                 break;
             //三分快3
             case 'sfk3':
                 $start     = strtotime(date("Y-m-d").' 00:00:00');//开始销售时间
                 $end       = strtotime(date("Y-m-d").' 24:00:00');//最后一期开奖时间
                 $steps     = 60*3;
                 $time = time();
                 $round = floor(($time-$start)/$steps);
                 //下期
                 $issue = date("Ymd").str_pad($round+1,3,'0',STR_PAD_LEFT);
                 $opentime = $start+($round*$steps)+$steps;
                 break;
             //五分快3
             case 'wfk3':
                 $start     = strtotime(date("Y-m-d").' 00:00:00');//开始销售时间
                 $end       = strtotime(date("Y-m-d").' 24:00:00');//最后一期开奖时间
                 $steps     = 60*5;
                 $time = time();
                 $round = floor(($time-$start)/$steps);
                 //下期
                 $issue = date("Ymd").str_pad($round+1,3,'0',STR_PAD_LEFT);
                 $opentime = $start+($round*$steps)+$steps;
                 break;

             case 'jsk3'://江苏快三
                 $total = 41;
                 $start     = strtotime(date("Y-m-d").' 08:40:00');//开始销售时间
                 $end       = strtotime(date("Y-m-d").' 22:10:00');//最后一期开奖时间
                 $steps     = 60*20;
                 $time = time();
                 //未到第一期
                 if($time<$start){
                     $opentime = $start-$time;
                     $issue    = date("Ymd",$time).'01';
                 }elseif($time>=$end){
                     $opentime = $start+86400;
                     $issue    = date("Ymd",$time+86400).'01';
                 }elseif($time>=$end){//结束到第二天
                     $opentime = $start+86400-$time;
                     $issue    = date("Ymd",$time).'01';
                 }else{
                     $round = floor(($time-$start)/$steps);
                     if($round<9){
                         $issue    = date("Ymd"). str_pad(0,2,$round+1);
                     }else{
                         $issue    = date("Ymd"). ($round+1);
                     }
                     $opentime = $start+($round*$steps)+$steps;
                 }
                 break;
             case 'ahk3'://安徽快三
                 $total = 40;
                 $start     = strtotime(date("Y-m-d").' 08:40:00');//开始销售时间
                 $end       = strtotime(date("Y-m-d").' 22:00:00');//最后一期开奖时间
                 $steps     = 60*20;
                 $time = time();
                 //未到第一期
                 if($time<$start){
                     $opentime = $start-$time;
                     $issue    = date("Ymd",$time).'01';
                 }elseif($time>=$end){
                     $opentime = $start+86400+$steps;
                     $issue    = date("Ymd",$time+86400).'01';
                 }else{
                     $round = floor(($time-$start)/$steps);
                     if($round<9){
                         $issue    = date("Ymd"). str_pad(0,2,$round+1);
                     }else{
                         $issue    = date("Ymd"). ($round+1);
                     }
                     $opentime = $start+($round*$steps)+$steps;
                 }
                 break;
             case 'bjk3'://北京快三，累加的
                 $total = 44;
                 $start     = strtotime(date("Y-m-d").' 09:00:00');//开始销售时间
                 $end       = strtotime(date("Y-m-d").' 23:40:00');//最后一期开奖时间
                 $steps     = 60*20;
                 $time = time();
                 $round = floor(($time-$start)/$steps);

                 break;
             case 'fjk3'://福建快三
                 $total = 42;
                 $start     = strtotime(date("Y-m-d").' 08:30:00');//开始销售时间
                 $end       = strtotime(date("Y-m-d").' 22:35:00');//最后一期开奖时间
                 $steps     = 60*20;
                 $time = time();

                 //未到第一期
                 if($time<$start){
                     $opentime = $start-$time;
                     $issue    = date("Ymd",$time).'01';
                 }elseif($time>=$end){
                     $opentime = $start+86400+$steps;
                     $issue    = date("Ymd",$time+86400).'01';
                 }else{
                     $round = floor(($time-$start)/$steps);
                     if($round<9){
                         $issue    = date("Ymd"). str_pad(0,2,$round+1);
                     }else{
                         $issue    = date("Ymd"). ($round+1);
                     }
                     $opentime = $start+($round*$steps)+$steps;
                 }
                 break;
             case 'gxk3'://广西快三
                 $total = 40;
                 $start     = strtotime(date("Y-m-d").' 09:10:00');//开始销售时间
                 $end       = strtotime(date("Y-m-d").' 22:30:00');//最后一期开奖时间
                 $steps     = 60*20;
                 $time = time();
                 //未到第一期
                 if($time<$start){
                     $opentime = $start-$time;
                     $issue    = date("Ymd",$time).'01';
                 }elseif($time>=$end){
                     $opentime = $start+86400+$steps;
                     $issue    = date("Ymd",$time+86400).'01';
                 }else{
                     $round = floor(($time-$start)/$steps);
                     if($round<9){
                         $issue    = date("Ymd"). str_pad(0,2,$round+1);
                     }else{
                         $issue    = date("Ymd"). ($round+1);
                     }
                     $opentime = $start+($round*$steps)+$steps;
                 }
                 break;
             case 'gzk3'://贵州快三
                 $total = 40;
                 $start     = strtotime(date("Y-m-d").' 09:10:00');//开始销售时间
                 $end       = strtotime(date("Y-m-d").' 22:35:00');//最后一期开奖时间
                 $steps     = 60*20;
                 $time = time();
                 //未到第一期
                 if($time<$start){
                     $opentime = $start-$time;
                     $issue    = date("Ymd",$time).'01';
                 }elseif($time>=$end){
                     $opentime = $start+86400+$steps;
                     $issue    = date("Ymd",$time+86400).'01';
                 }else{
                     $round = floor(($time-$start)/$steps);
                     if($round<9){
                         $issue    = date("Ymd"). str_pad(0,2,$round+1);
                     }else{
                         $issue    = date("Ymd"). ($round+1);
                     }
                     $opentime = $start+($round*$steps)+$steps;
                 }
                 break;
             case 'hbk3'://河北快三
                 $total = 41;
                 $start     = strtotime(date("Y-m-d").' 08:30:00');//开始销售时间
                 $end       = strtotime(date("Y-m-d").' 22:10:00');//最后一期开奖时间
                 $steps     = 60*20;
                 $time = time();
                 //未到第一期
                 if($time<$start){
                     $opentime = $start-$time;
                     $issue    = date("Ymd",$time).'01';
                 }elseif($time>=$end){
                     $opentime = $start+86400+$steps;
                     $issue    = date("Ymd",$time+86400).'01';
                 }else{
                     $round = floor(($time-$start)/$steps);
                     if($round<9){
                         $issue    = date("Ymd"). str_pad(0,2,$round+1);
                     }else{
                         $issue    = date("Ymd"). ($round+1);
                     }
                     $opentime = $start+($round*$steps)+$steps;
                 }
                 break;

             case 'hnk3'://河南快三
                 $total = 42;
                 $start     = strtotime(date("Y-m-d").' 08:30:00');//开始销售时间
                 $end       = strtotime(date("Y-m-d").' 22:30:00');//最后一期开奖时间
                 $steps     = 60*20;
                 $time = time();
                 //未到第一期
                 if($time<$start){
                     $opentime = $start-$time;
                     $issue    = date("Ymd",$time).'01';
                 }elseif($time>=$end){
                     $opentime = $start+86400+$steps;
                     $issue    = date("Ymd",$time+86400).'01';
                 }else{
                     $round = floor(($time-$start)/$steps);
                     if($round<9){
                         $issue    = date("Ymd"). str_pad(0,2,$round+1);
                     }else{
                         $issue    = date("Ymd"). ($round+1);
                     }
                     $opentime = $start+($round*$steps)+$steps;
                 }
                 break;

             case 'hubk3'://湖北快三
                 $total = 39;
                 $start     = strtotime(date("Y-m-d").' 09:00:00');//开始销售时间
                 $end       = strtotime(date("Y-m-d").' 22:00:00');//最后一期开奖时间
                 $steps     = 60*20;
                 $time = time();
                 //未到第一期
                 if($time<$start){
                     $opentime = $start-$time;
                     $issue    = date("Ymd",$time).'01';
                 }elseif($time>=$end){
                     $opentime = $start+86400+$steps;
                     $issue    = date("Ymd",$time+86400).'01';
                 }else{
                     $round = floor(($time-$start)/$steps);
                     if($round<9){
                         $issue    = date("Ymd"). str_pad(0,2,$round+1);
                     }else{
                         $issue    = date("Ymd"). ($round+1);
                     }
                     $opentime = $start+($round*$steps)+$steps;
                 }
                 break;

             case 'jlk3'://吉林快三
                 $total = 39;
                 $start     = strtotime(date("Y-m-d").' 09:00:00');//开始销售时间
                 $end       = strtotime(date("Y-m-d").' 22:00:00');//最后一期开奖时间
                 $steps     = 60*20;
                 $time = time();
                 //未到第一期
                 if($time<$start){
                     $opentime = $start-$time;
                     $issue    = date("Ymd",$time).'01';
                 }elseif($time>=$end){
                     $opentime = $start+86400+$steps;
                     $issue    = date("Ymd",$time+86400).'01';
                 }else{
                     $round = floor(($time-$start)/$steps);
                     if($round<9){
                         $issue    = date("Ymd"). str_pad(0,2,$round+1);
                     }else{
                         $issue    = date("Ymd"). ($round+1);
                     }
                     $opentime = $start+($round*$steps)+$steps;
                 }
                 break;
             case 'jxk3'://江西快三
                 $total = 42;
                 $start     = strtotime(date("Y-m-d").' 08:10:00');//开始销售时间
                 $end       = strtotime(date("Y-m-d").' 22:10:00');//最后一期开奖时间
                 $steps     = 60*20;
                 $time = time();
                 //未到第一期
                 if($time<$start){
                     $opentime = $start-$time;
                     $issue    = date("Ymd",$time).'01';
                 }elseif($time>=$end){
                     $opentime = $start+86400+$steps;
                     $issue    = date("Ymd",$time+86400).'01';
                 }else{
                     $round = floor(($time-$start)/$steps);
                     if($round<9){
                         $issue    = date("Ymd"). str_pad(0,2,$round+1);
                     }else{
                         $issue    = date("Ymd"). ($round+1);
                     }
                     $opentime = $start+($round*$steps)+$steps;
                 }
                 break;
             case 'nmk3'://内蒙快三
                 $total = 36;
                 $start     = strtotime(date("Y-m-d").' 09:40:00');//开始销售时间
                 $end       = strtotime(date("Y-m-d").' 21:40:00');//最后一期开奖时间
                 $steps     = 60*20;
                 $time = time();
                 //未到第一期
                 if($time<$start){
                     $opentime = $start-$time;
                     $issue    = date("Ymd",$time).'01';
                 }elseif($time>=$end){
                     $opentime = $start+86400+$steps;
                     $issue    = date("Ymd",$time+86400).'01';
                 }else{
                     $round = floor(($time-$start)/$steps);
                     if($round<9){
                         $issue    = date("Ymd"). str_pad(0,2,$round+1);
                     }else{
                         $issue    = date("Ymd"). ($round+1);
                     }
                     $opentime = $start+($round*$steps)+$steps;
                 }
                 break;
             case 'nxk3'://宁夏快三
                 $total = 39;
                 $start     = strtotime(date("Y-m-d").' 08:55:00');//开始销售时间
                 $end       = strtotime(date("Y-m-d").' 21:55:00');//最后一期开奖时间
                 $steps     = 60*20;
                 $time = time();
                 //未到第一期
                 if($time<$start){
                     $opentime = $start-$time;
                     $issue    = date("Ymd",$time).'01';
                 }elseif($time>=$end){
                     $opentime = $start+86400+$steps;
                     $issue    = date("Ymd",$time+86400).'01';
                 }else{
                     $round = floor(($time-$start)/$steps);
                     if($round<9){
                         $issue    = date("Ymd"). str_pad(0,2,$round+1);
                     }else{
                         $issue    = date("Ymd"). ($round+1);
                     }
                     $opentime = $start+($round*$steps)+$steps;
                 }
                 break;
             case 'qhk3'://青海快三,会提前开
                 $total = 39;
                 $start     = strtotime(date("Y-m-d").' 09:00:00');//开始销售时间
                 $end       = strtotime(date("Y-m-d").' 21:55:00');//最后一期开奖时间
                 $steps     = 60*20;
                 $time = time();
                 //未到第一期
                 if($time<$start){
                     $opentime = $start-$time;
                     $issue    = date("Ymd",$time).'01';
                 }elseif($time>=$end){
                     $opentime = $start+86400+$steps;
                     $issue    = date("Ymd",$time+86400).'01';
                 }else{
                     $round = floor(($time-$start)/$steps);
                     if($round<9){
                         $issue    = date("Ymd"). str_pad(0,2,$round+1);
                     }else{
                         $issue    = date("Ymd"). ($round+1);
                     }
                     $opentime = $start+($round*$steps)+$steps;
                 }
                 break;
             case 'shk3'://上海快三
                 $total = 41;
                 $start     = strtotime(date("Y-m-d").' 08:35:00');//开始销售时间
                 $end       = strtotime(date("Y-m-d").' 22:15:00');//最后一期开奖时间
                 $steps     = 60*20;
                 $time = time();
                 //未到第一期
                 if($time<$start){
                     $opentime = $start-$time;
                     $issue    = date("Ymd",$time).'01';
                 }elseif($time>=$end){
                     $opentime = $start+86400+$steps;
                     $issue    = date("Ymd",$time+86400).'01';
                 }else{
                     $round = floor(($time-$start)/$steps);
                     if($round<9){
                         $issue    = date("Ymd"). str_pad(0,2,$round+1);
                     }else{
                         $issue    = date("Ymd"). ($round+1);
                     }
                     $opentime = $start+($round*$steps)+$steps;
                 }
                 break;

             case 'xzk3'://西藏快三
                 $total = 42;
                 $start     = strtotime(date("Y-m-d").' 09:20:00');//开始销售时间
                 $end       = strtotime(date("Y-m-d").' 23:20:00');//最后一期开奖时间
                 $steps     = 60*20;
                // echo  date("Y-m-d H:i:s",$start+$steps*$total);die;
                 $time = time();
                 //未到第一期
                 if($time<$start){
                     $opentime = $start-$time;
                     $issue    = date("Ymd",$time).'01';
                 }elseif($time>=$end){
                     $opentime = $start+86400+$steps;
                     $issue    = date("Ymd",$time+86400).'01';
                 }else{
                     $round = floor(($time-$start)/$steps);
                     if($round<9){
                         $issue    = date("Ymd"). str_pad(0,2,$round+1);
                     }else{
                         $issue    = date("Ymd"). ($round+1);
                     }
                     $opentime = $start+($round*$steps)+$steps;
                 }
                 break;


             //一分快乐8
             case 'yfklb':
                 $start     = strtotime(date("Y-m-d").' 00:00:00');//开始销售时间
                 $end       = strtotime(date("Y-m-d").' 24:00:00');//最后一期开奖时间
                 $steps     = 60*1;
                 $time = time();
                 $round = floor(($time-$start)/$steps);
                 //下期
                 $issue    = date("Ymd"). str_pad(0,4,$round+1);
                 $opentime = $start+($round*$steps)+$steps;

                 break;
             //三分快乐8
             case 'sfklb':
                 $start     = strtotime(date("Y-m-d").' 00:00:00');//开始销售时间
                 $end       = strtotime(date("Y-m-d").' 24:00:00');//最后一期开奖时间
                 $steps     = 60*3;
                 $time = time();
                 $round = floor(($time-$start)/$steps);
                 //下期
                 $issue = date("Ymd").str_pad($round+1,3,'0',STR_PAD_LEFT);
                 $opentime = $start+($round*$steps)+$steps;
                 break;
             //五分快乐8
             case 'wfklb':
                 $start     = strtotime(date("Y-m-d").' 00:00:00');//开始销售时间
                 $end       = strtotime(date("Y-m-d").' 24:00:00');//最后一期开奖时间
                 $steps     = 60*5;
                 $time = time();
                 $round = floor(($time-$start)/$steps);
                 //下期
                 $issue = date("Ymd").str_pad($round+1,3,'0',STR_PAD_LEFT);
                 $opentime = $start+($round*$steps)+$steps;
                 break;

             case 'bjklb'://北京快乐8，期号采用累加
                 $total = 179;
                 $start     = strtotime(date("Y-m-d").' 09:00:00');//开始销售时间
                 $end       = strtotime(date("Y-m-d").' 23:55:00');//最后一期开奖时间
                 $steps     = 60*5;
              //  echo  date("Y-m-d H:i:s",$start+$steps*$total);die;
                 $time = time();
                 //未到第一期
                 if($time<$start){
                     $opentime = $start-$time;
                     $round = 1;
                 }elseif($time>=$end){
                     $opentime = $start+86400+$steps;
                     $round = 1;
                 }else{
                     $round = floor(($time-$start)/$steps);
                     $opentime = $start+($round*$steps)+$steps;
                 }
                 break;
             case 'jndklb'://加拿大快乐8，期号采用累加
                 $total = 338;
                 $start     = strtotime(date("Y-m-d").' 00:00:00');//开始销售时间
                 $end       = $start+86400;//最后一期开奖时间
                 $steps     = 210;
                 $time = time();
                 //未到第一期
                 if($time<$start){
                     $opentime = $start-$time;
                     $round = 1;
                 }elseif($time>=$end){
                     $opentime = $start+86400+$steps;
                     $round = 1;
                 }else{
                     $round = floor(($time-$start)/$steps);
                     $opentime = $start+($round*$steps)+$steps;
                 }
                 break;



             //一分六合彩
             case 'yflhc':
                 $start     = strtotime(date("Y-m-d").' 00:00:00');//开始销售时间
                 $end       = strtotime(date("Y-m-d").' 24:00:00');//最后一期开奖时间
                 $steps     = 60*1;
                 $time = time();
                 $round = floor(($time-$start)/$steps);
                 //下期
                 $issue    = date("Ymd"). str_pad(0,4,$round+1);
                 $opentime = $start+($round*$steps)+$steps;

                 break;
             //三分六合彩
             case 'sflhc':
                 $start     = strtotime(date("Y-m-d").' 00:00:00');//开始销售时间
                 $end       = strtotime(date("Y-m-d").' 24:00:00');//最后一期开奖时间
                 $steps     = 60*3;
                 $time = time();
                 $round = floor(($time-$start)/$steps);
                 //下期
                 $issue = date("Ymd").str_pad($round+1,3,'0',STR_PAD_LEFT);
                 $opentime = $start+($round*$steps)+$steps;
                 break;
             //五分六合彩
             case 'wflhc':
                 $start     = strtotime(date("Y-m-d").' 00:00:00');//开始销售时间
                 $end       = strtotime(date("Y-m-d").' 24:00:00');//最后一期开奖时间
                 $steps     = 60*5;
                 $time = time();
                 $round = floor(($time-$start)/$steps);
                 //下期
                 $issue = date("Ymd").str_pad($round+1,3,'0',STR_PAD_LEFT);
                 $opentime = $start+($round*$steps)+$steps;
                 break;

             default:
                 $reponse = array();
         }
        $reponse = array(
            'issue'=>$issue,
            'opentime'=>$opentime,
            'format'=>date("Y-m-d H:i:s",$opentime)
        );
            var_dump($reponse);
         return $reponse;
    }
}