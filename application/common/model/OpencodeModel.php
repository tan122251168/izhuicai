<?php
/**
 * Created by PhpStorm.
 * User: lenovo
 * Date: 2020/4/4
 * Time: 8:18
 */

namespace app\common\model;


use think\Model;

class OpencodeModel extends Model
{
    protected $table='opencode';

    public function findOne(array $where){
        return self::where($where)->find();
    }
}