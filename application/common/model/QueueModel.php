<?php
/**
 * Created by PhpStorm.
 * User: lenovo
 * Date: 2020/3/19
 * Time: 23:30
 */
namespace app\common\model;

class QueueModel
{
    /**加入队列
     * @param $key队列键
     * @param $value队列值
     * @return int
     */
    public static function push($key,$value){
        $redis = new \Redis();
        $redis->connect('127.0.0.1',6379);
        return  $redis->rpush($key,$value);
    }

    /**消费队列信息
     * @param $key队列键
     * @return int
     */
    public  static  function consume($key){
        $redis = new \Redis();
        $redis->connect('127.0.0.1',6379);
        return  $redis->lpop($key);
    }
}