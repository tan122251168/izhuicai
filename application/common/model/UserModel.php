<?php
/**
 * Created by PhpStorm.
 * User: lenovo
 * Date: 2020/3/24
 * Time: 22:49
 */

namespace app\common\model;


use think\Model;

class UserModel extends Model
{
    /**
     * 数据表名称
     * @var string
     */
    protected $table='user';
}