<?php
/**
 * Created by PhpStorm.
 * User: lenovo
 * Date: 2020/3/23
 * Time: 21:21
 */

namespace app\common\model;

use QL\QueryList;
class Www1680380Model
{
     public $url = 'https://api.api861861.com/';
    /**抓取彩票数据
     * @param $code
     * @param int $limit
     * @return mixed
     */
    public function queryLottery($code){
        switch ($code){
            case 'bjsc'://北京赛车
                $url = $this->url.'ElevenFive/getElevenFiveList.do?date=&lotCode=10001';
                break;
            case 'xyft'://幸运飞艇
                $url = $this->url.'ElevenFive/getElevenFiveList.do?date=&lotCode=10057';
                break;
            case 'cqssc'://重庆时时彩
                $url = $this->url.'ElevenFive/getElevenFiveList.do?date=&lotCode=10060';
                break;
            case 'tjssc'://天津时时彩
                $url = $this->url.'ElevenFive/getElevenFiveList.do?date=&lotCode=10003';
                break;
            case 'xjssc'://新疆时时彩
                $url = $this->url.'ElevenFive/getElevenFiveList.do?date=&lotCode=10004';
                break;
            case 'jsk3'://江苏快三
                $url = $this->url.'ElevenFive/getElevenFiveList.do?date=&lotCode=10007';
                break;
            case 'hbk3'://河北快三
                $url = $this->url.'ElevenFive/getElevenFiveList.do?date=&lotCode=10028';
                break;
            case 'ahk3'://安徽快三
                $url = $this->url.'ElevenFive/getElevenFiveList.do?date=&lotCode=10030';
                break;
            case 'nmgk3'://内蒙古快三
                $url = $this->url.'ElevenFive/getElevenFiveList.do?date=&lotCode=10029';
                break;
            case 'fjk3'://福建快三
                $url = $this->url.'ElevenFive/getElevenFiveList.do?date=&lotCode=10031';
                break;
            case 'hubk3'://湖北快三
                $url = $this->url.'ElevenFive/getElevenFiveList.do?date=&lotCode=10032';
                break;
            case 'bjk3'://北京快三
                $url = $this->url.'ElevenFive/getElevenFiveList.do?date=&lotCode=10033';
                break;
            case 'gxk3'://广西快三
                $url = $this->url.'ElevenFive/getElevenFiveList.do?date=&lotCode=10026';
                break;
            case 'shk3'://上海快三
                $url = $this->url.'ElevenFive/getElevenFiveList.do?date=&lotCode=10061';
                break;
            case 'gzk3'://贵州快三
                $url = $this->url.'ElevenFive/getElevenFiveList.do?date=&lotCode=10062';
                break;
            case 'gsk3'://甘肃快三
                $url = $this->url.'ElevenFive/getElevenFiveList.do?date=&lotCode=10063';
                break;
            case 'jlk3'://吉林快三
                $url = $this->url.'ElevenFive/getElevenFiveList.do?date=&lotCode=10027';
                break;
            case 'gd11x5'://广东11选5
                $url = $this->url.'ElevenFive/getElevenFiveList.do?date=&lotCode=10006';
                break;
            case 'sh11x5'://上海11选5
                $url = $this->url.'ElevenFive/getElevenFiveList.do?date=&lotCode=10018';
                break;
            case 'ah11x5'://安徽11选5
                $url = $this->url.'ElevenFive/getElevenFiveList.do?date=&lotCode=10017';
                break;
            case 'jx11x5'://江西11选5
                $url = $this->url.'ElevenFive/getElevenFiveList.do?date=&lotCode=10015';
                break;
            case 'hub11x5'://湖北11选5
                $url = $this->url.'ElevenFive/getElevenFiveList.do?date=&lotCode=10020';
                break;
            case 'ln11x5'://辽宁11选5
                $url = $this->url.'ElevenFive/getElevenFiveList.do?date=&lotCode=10019';
                break;
            case 'js11x5'://江苏11选5
                $url = $this->url.'ElevenFive/getElevenFiveList.do?date=&lotCode=10016';
                break;
            case 'zj11x5'://浙江11选5
                $url = $this->url.'ElevenFive/getElevenFiveList.do?date=&lotCode=10025';
                break;
            case 'nmg11x5'://内蒙古11选5
                $url = $this->url.'ElevenFive/getElevenFiveList.do?date=&lotCode=10024';
                break;
        }

        $ql = QueryList::get(
            $url
        );
        $html = (string)$ql->getHtml();
        return json_decode( $html,true);
    }
}