<?php
/**http://izhuicai.cn/index.php/index/api/query
 * Created by Benjiemin
 * Date: 2020/3/24
 * Time: 17:22
 */

namespace app\index\controller;


use app\index\model\StatusModel;
use app\index\model\UserModel;
use think\cache\driver\Redis;
use think\Controller;
use app\index\model\LotteryTypesModel;
use yii\helpers\Json;

class Api extends Controller
{
    private $last_visit = 'api_query_';
    private $user = '';

    protected $beforeActionList = [
        'getUserInfo',
    ];

    protected function getUserInfo(){
        $token = input('token','0');
        $userModel = new UserModel();
        $userInfo = $userModel->where(['token'=>$token])->find();
        if(!empty($userInfo)){
            $this->user = $userInfo;
        }
    }

    /**
     * 当前投注期号
     */
    public function actionCurrentIssue(){
        $token = input('token');
        $code  =  input('code');//彩票代码
        $limit = 3;
        $id = 1;
        $key = $this->last_visit.$id.$code;
        $redisModel = new Redis();
        $lastTime = $redisModel->get($key);
        if((time()-$lastTime)<$limit){
            $reponse = array(
                'msg'=>"请求超频,最快可以{$limit}秒请求一次，建议每期请求一次就缓存或者入库数据，避免频繁请求",
                'data'=>array(),
                'error'=>1
            );
            return json($reponse,StatusModel::CODE_FORBIDDEN);
        }

        $lotteryTypesModel = new LotteryTypesModel();
        $nextIssue = $lotteryTypesModel->nextIssue($code);
        $reponse = array(
            'msg'=>"查询成功",
            'data'=>$nextIssue,
            'error'=>0
        );
        return json( $reponse);
    }
}