<?php
namespace app\index\controller;

use app\index\model\LotteryTypesModel;
use think\cache\driver\Redis;

class Index
{
    public function index()
    {
        $lotteryTypesModel = new LotteryTypesModel();
        $lotteryTypesModel->nextIssue('sfpk10');
    }
}
