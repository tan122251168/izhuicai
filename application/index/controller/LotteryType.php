<?php
/**
 * Created by PhpStorm.
 * User: lenovo
 * Date: 2020/4/3
 * Time: 22:19
 */

namespace app\index\controller;


use app\common\model\LotteryTypeModel;
use app\common\model\StatusModel;

class LotteryType extends Base
{
    /**
     * 获取全部分类
     */
    public function index(){
        $lotteryTypeModel = new LotteryTypeModel();
        $list = $lotteryTypeModel->findByParentId(0);
        foreach ($list as &$item){
            $childList = $lotteryTypeModel->findByParentId($item['id']);
            $item['child'] = $childList;
        }
        $reponse = array(
            'msg'=>"查询成功",
            'data'=>$list,
            'status'=>StatusModel::CODE_OK
        );
        return json( $reponse);
    }

    /**
     * 获取子类型,父级分类
     */
    public function type(){
        $parentId = input('parent_id',0);
        $lotteryTypeModel = new LotteryTypeModel();
        $list = $lotteryTypeModel->findByParentId($parentId);
        $reponse = array(
            'msg'=>"查询成功",
            'data'=>$list,
            'status'=>StatusModel::CODE_OK
        );
        return json( $reponse);
    }



}