<?php
/**
 * Created by PhpStorm.
 * User: lenovo
 * Date: 2020/4/4
 * Time: 8:19
 */

namespace app\index\controller;


use app\common\model\OpencodeModel;
use app\common\model\StatusModel;
class Opencode extends Base
{
    /**
     * 开奖信息
     */
    public function index(){
        $size            = (int)input('size',30);
        $type_id =  (int)input('type_id');
        $where['lottery_type_id'] = $type_id;
        $opencodeModel = new OpencodeModel();
        $list = $opencodeModel->where($where)->order('issue desc')->paginate($size);

        $reponse = $list;
        $reponse['msg'] = "查询成功";
        $reponse['status'] = StatusModel::CODE_OK;
        return json( $reponse);
    }
}