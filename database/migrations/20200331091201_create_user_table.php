<?php

use think\migration\Migrator;
use think\migration\db\Column;

class CreateUserTable extends Migrator
{

    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        // create the table
        $table = $this->table('user',array('engine'=>'InnoDB','comment'=>'用户表'));
        $table
            ->addColumn('parent_id', 'integer',array('limit' => 11,'default'=>0,'comment'=>'父级ID'))
            ->addColumn('user_group_id', 'integer',array('limit' => 11,'default'=>0,'comment'=>'用户组ID'))
            ->addColumn('username', 'string',array('limit' => 15,'default'=>'','comment'=>'用户名'))
            ->addColumn('mobile', 'integer',array('limit' => 11,'default'=>0,'comment'=>'手机号'))
            ->addColumn('balance', 'decimal',array('precision' => 12,'scale'=>2,'default'=>0,'comment'=>'余额'))
            ->addColumn('total_recharge', 'decimal',array('precision' => 12,'scale'=>2,'default'=>0,'comment'=>'总充值'))
            ->addColumn('total_withdraw', 'decimal',array('precision' => 12,'scale'=>2,'default'=>0,'comment'=>'总提现'))
            ->addColumn('last_login_ip', 'integer',array('limit' => 11,'default'=>0,'comment'=>'最后登录IP'))
            ->addColumn('last_login_time', 'datetime',array('comment'=>'最后登录时间'))
            ->addColumn('is_delete', 'boolean',array('limit' => 1,'default'=>0,'comment'=>'删除状态，1已删除'))
            ->addColumn('create_time', 'datetime',array('comment'=>'创建时间'))
            ->addColumn('update_time', 'datetime',array('comment'=>'更新时间'))
            ->addIndex(array('username'), array('unique' => true))
            ->create();
    }
}
