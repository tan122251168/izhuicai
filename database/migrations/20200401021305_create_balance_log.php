<?php

use think\migration\Migrator;
use think\migration\db\Column;

class CreateBalanceLog extends Migrator
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('balance_log',array('engine'=>'InnoDB','comment'=>'资金记录'));
        $table->addColumn('user_id', 'integer', array('limit' => 11,'default'=>'0','comment'=>'用户id'))
            ->addColumn('ordernum', 'integer',array('limit' => 22,'default'=>0,'comment'=>'订单号'))
            ->addColumn('relevancy_ordernum', 'integer',array('limit' => 22,'default'=>0,'comment'=>'关联订单号'))
            ->addColumn('btype', 'integer',array('limit' => 1,'default'=>0,'comment'=>'变动类型：1入款，2出款'))
            ->addColumn('stype', 'integer',array('limit' => 2,'default'=>0,'comment'=>'子类型：1存款，2，提款，3.下注扣款，4派彩票，5.代理提成，6.红包，7.退款'))
            ->addColumn('balance_before', 'decimal',array('precision' => 12,'scale'=>2,'default'=>0,'comment'=>'变动前余额'))
            ->addColumn('balance', 'decimal',array('precision' => 12,'scale'=>2,'default'=>0,'comment'=>'变动金额'))
            ->addColumn('balance_after', 'decimal',array('precision' => 12,'scale'=>2,'default'=>0,'comment'=>'变动后余额'))
            ->addColumn('create_time', 'datetime',array('comment'=>'创建时间'))
            ->addColumn('update_time', 'datetime',array('comment'=>'更新时间'))
            ->addIndex(array('ordernum'), array('unique' => true))
            ->create();
    }
}
