<?php

use think\migration\Migrator;
use think\migration\db\Column;

class UserLog extends Migrator
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('user_log',array('engine'=>'InnoDB','comment'=>'用户日志'));
        $table
            ->addColumn('user_id', 'integer',array('limit' => 11,'default'=>0,'comment'=>'用户id'))
            ->addColumn('action', 'integer',array('limit' =>2,'default'=>'0','comment'=>'动作：1登录，2退出，3下单，4充值，5提现,6其他'))
            ->addColumn('msg', 'string',array('limit' => 32,'default'=>'0','comment'=>'信息'))
            ->addColumn('create_time', 'datetime',array('comment'=>'创建时间'))
            ->addColumn('update_time', 'datetime',array('comment'=>'更新时间'))
            ->addIndex(array('user_id'))
            ->create();
    }
}
