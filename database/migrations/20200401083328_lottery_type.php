<?php

use think\migration\Migrator;
use think\migration\db\Column;

class LotteryType extends Migrator
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('lottery_type',array('engine'=>'InnoDB','comment'=>'彩票分类'));
        $table
            ->addColumn('code', 'string',array('limit' =>10,'default'=>0,'comment'=>'彩票代码'))
            ->addColumn('parent_id', 'integer',array('limit' =>11,'default'=>'0','comment'=>'父级ID'))
            ->addColumn('is_sys', 'integer',array('limit' =>1,'default'=>'0','comment'=>'是否系统彩：1是，0不是'))
            ->addColumn('cn_name', 'string',array('limit' => 32,'default'=>'0','comment'=>'中文名'))
            ->addColumn('diy_name', 'string',array('limit' => 32,'default'=>'0','comment'=>'自定义名字'))
            ->addColumn('description', 'string',array('limit' => 50,'default'=>'0','comment'=>'描述'))
            ->addColumn('remark', 'string',array('limit' => 32,'default'=>'0','comment'=>'备注'))
            ->addColumn('icon', 'string',array('limit' => 200,'default'=>'0','comment'=>'图标'))
            ->addColumn('is_delete', 'boolean',array('limit' => 1,'default'=>0,'comment'=>'删除状态，1已删除，0正常'))
            ->addColumn('create_time', 'datetime',array('comment'=>'创建时间'))
            ->addColumn('update_time', 'datetime',array('comment'=>'更新时间'))
            ->addIndex(array('code'), array('unique' => true))
            ->create();

     //初始化数据
        //插入大分类
        $data = [
            ['code'=>'ssc','parent_id'=>0,'cn_name'=>'时时彩','diy_name'=>'时时彩','description'=>'时时彩','remark'=>'无','icon'=>'/sadfa/dasf.png','is_delete'=>0,'create_time'=>timeFormat(time()),'update_time'=>timeFormat(time())],
            ['code'=>'lhc','parent_id'=>0,'cn_name'=>'六合彩','diy_name'=>'六合彩','description'=>'六合彩','remark'=>'无','icon'=>'/sadfa/dasf.png','is_delete'=>0,'create_time'=>timeFormat(time()),'update_time'=>timeFormat(time())],
            ['code'=>'pk10','parent_id'=>0,'cn_name'=>'PK10','diy_name'=>'PK10','description'=>'PK10','remark'=>'无','icon'=>'/sadfa/dasf.png','is_delete'=>0,'create_time'=>timeFormat(time()),'update_time'=>timeFormat(time())],
            ['code'=>'klsf','parent_id'=>0,'cn_name'=>'快乐10分','diy_name'=>'快乐10分','description'=>'快乐10分','remark'=>'无','icon'=>'/sadfa/dasf.png','is_delete'=>0,'create_time'=>timeFormat(time()),'update_time'=>timeFormat(time())],
            ['code'=>'kuai3','parent_id'=>0,'cn_name'=>'快3','diy_name'=>'快3','description'=>'快3','remark'=>'无','icon'=>'/sadfa/dasf.png','is_delete'=>0,'create_time'=>timeFormat(time()),'update_time'=>timeFormat(time())],
            ['code'=>'11x5','parent_id'=>0,'cn_name'=>'11选5','diy_name'=>'11选5','description'=>'11选5','remark'=>'无','icon'=>'/sadfa/dasf.png','is_delete'=>0,'create_time'=>timeFormat(time()),'update_time'=>timeFormat(time())],
            ['code'=>'klb','parent_id'=>0,'cn_name'=>'快乐8','diy_name'=>'快乐8','description'=>'快乐8','remark'=>'无','icon'=>'/sadfa/dasf.png','is_delete'=>0,'create_time'=>timeFormat(time()),'update_time'=>timeFormat(time())],
            ['code'=>'xy28','parent_id'=>0,'cn_name'=>'幸运28','diy_name'=>'幸运28','description'=>'幸运28','remark'=>'无','icon'=>'/sadfa/dasf.png','is_delete'=>0,'create_time'=>timeFormat(time()),'update_time'=>timeFormat(time())],
        ];
        $this->insert('lottery_type', $data);
        $sql = "SELECT * FROM `lottery_type` WHERE code='ssc'";
        $info = $this->fetchRow($sql);

        $data = [
            ['code'=>'30ssc','parent_id'=>$info['id'],'is_sys'=>1,'cn_name'=>'极速时时彩','diy_name'=>'极速时时彩','description'=>'极速时时彩','remark'=>'无','icon'=>'/sadfa/dasf.png','is_delete'=>0,'create_time'=>timeFormat(time()),'update_time'=>timeFormat(time())],
            ['code'=>'yfjssc','parent_id'=>$info['id'],'is_sys'=>1,'cn_name'=>'一分时时彩','diy_name'=>'一分时时彩','description'=>'一分时时彩','remark'=>'无','icon'=>'/sadfa/dasf.png','is_delete'=>0,'create_time'=>timeFormat(time()),'update_time'=>timeFormat(time())],
            ['code'=>'sfjssc','parent_id'=>$info['id'],'is_sys'=>1,'cn_name'=>'三分时时彩','diy_name'=>'三分时时彩','description'=>'三分时时彩','remark'=>'无','icon'=>'/sadfa/dasf.png','is_delete'=>0,'create_time'=>timeFormat(time()),'update_time'=>timeFormat(time())],
            ['code'=>'wfssc','parent_id'=>$info['id'],'is_sys'=>1,'cn_name'=>'五分时时彩','diy_name'=>'五分时时彩','description'=>'五分时时彩','remark'=>'无','icon'=>'/sadfa/dasf.png','is_delete'=>0,'create_time'=>timeFormat(time()),'update_time'=>timeFormat(time())],
            ['code'=>'10jssc','parent_id'=>$info['id'],'is_sys'=>1,'cn_name'=>'十分时时彩','diy_name'=>'十分时时彩','description'=>'十分时时彩','remark'=>'无','icon'=>'/sadfa/dasf.png','is_delete'=>0,'create_time'=>timeFormat(time()),'update_time'=>timeFormat(time())],
            ['code'=>'hljssc','parent_id'=>$info['id'],'cn_name'=>'黑龙江时时彩','diy_name'=>'黑龙江时时彩','description'=>'黑龙江时时彩','remark'=>'无','icon'=>'/sadfa/dasf.png','is_delete'=>0,'create_time'=>timeFormat(time()),'update_time'=>timeFormat(time())],
            ['code'=>'hbssc','parent_id'=>$info['id'],'cn_name'=>'湖北时时彩','diy_name'=>'湖北时时彩','description'=>'湖北时时彩','remark'=>'无','icon'=>'/sadfa/dasf.png','is_delete'=>0,'create_time'=>timeFormat(time()),'update_time'=>timeFormat(time())],
            ['code'=>'jlssc','parent_id'=>$info['id'],'cn_name'=>'吉林时时彩','diy_name'=>'吉林时时彩','description'=>'吉林时时彩','remark'=>'无','icon'=>'/sadfa/dasf.png','is_delete'=>0,'create_time'=>timeFormat(time()),'update_time'=>timeFormat(time())],
            ['code'=>'nmgssc','parent_id'=>$info['id'],'cn_name'=>'内蒙古时时彩','diy_name'=>'内蒙古时时彩','description'=>'内蒙古时时彩','remark'=>'无','icon'=>'/sadfa/dasf.png','is_delete'=>0,'create_time'=>timeFormat(time()),'update_time'=>timeFormat(time())],
            ['code'=>'tjssc','parent_id'=>$info['id'],'cn_name'=>'天津时时彩','diy_name'=>'天津时时彩','description'=>'天津时时彩','remark'=>'无','icon'=>'/sadfa/dasf.png','is_delete'=>0,'create_time'=>timeFormat(time()),'update_time'=>timeFormat(time())],
            ['code'=>'xjssc','parent_id'=>$info['id'],'cn_name'=>'新疆时时彩','diy_name'=>'新疆时时彩','description'=>'新疆时时彩','remark'=>'无','icon'=>'/sadfa/dasf.png','is_delete'=>0,'create_time'=>timeFormat(time()),'update_time'=>timeFormat(time())],
            ['code'=>'ynssc','parent_id'=>$info['id'],'cn_name'=>'云南时时彩','diy_name'=>'云南时时彩','description'=>'云南时时彩','remark'=>'无','icon'=>'/sadfa/dasf.png','is_delete'=>0,'create_time'=>timeFormat(time()),'update_time'=>timeFormat(time())],
            ['code'=>'cqssc','parent_id'=>$info['id'],'cn_name'=>'重庆时时彩','diy_name'=>'重庆时时彩','description'=>'重庆时时彩','remark'=>'无','icon'=>'/sadfa/dasf.png','is_delete'=>0,'create_time'=>timeFormat(time()),'update_time'=>timeFormat(time())],
        ];
        $this->insert('lottery_type', $data);
        //六合彩数据
        $sql = "SELECT * FROM `lottery_type` WHERE code='lhc'";
        $info = $this->fetchRow($sql);
        $data = [
            ['code'=>'30lhc','parent_id'=>$info['id'],'is_sys'=>1,'cn_name'=>'极速六合彩','diy_name'=>'极速六合彩','description'=>'极速六合彩','remark'=>'无','icon'=>'/sadfa/dasf.png','is_delete'=>0,'create_time'=>timeFormat(time()),'update_time'=>timeFormat(time())],
            ['code'=>'yflhc','parent_id'=>$info['id'],'is_sys'=>1,'cn_name'=>'一分六合彩','diy_name'=>'一分六合彩','description'=>'一分六合彩','remark'=>'无','icon'=>'/sadfa/dasf.png','is_delete'=>0,'create_time'=>timeFormat(time()),'update_time'=>timeFormat(time())],
            ['code'=>'sflhc','parent_id'=>$info['id'],'is_sys'=>1,'cn_name'=>'三分六合彩','diy_name'=>'三分六合彩','description'=>'三分六合彩','remark'=>'无','icon'=>'/sadfa/dasf.png','is_delete'=>0,'create_time'=>timeFormat(time()),'update_time'=>timeFormat(time())],
            ['code'=>'wflhc','parent_id'=>$info['id'],'is_sys'=>1,'cn_name'=>'五分六合彩','diy_name'=>'五分六合彩','description'=>'五分六合彩','remark'=>'无','icon'=>'/sadfa/dasf.png','is_delete'=>0,'create_time'=>timeFormat(time()),'update_time'=>timeFormat(time())],
            ['code'=>'shflhc','parent_id'=>$info['id'],'is_sys'=>1,'cn_name'=>'十分六合彩','diy_name'=>'十分六合彩','description'=>'十分六合彩','remark'=>'无','icon'=>'/sadfa/dasf.png','is_delete'=>0,'create_time'=>timeFormat(time()),'update_time'=>timeFormat(time())],
            ['code'=>'xglhc','parent_id'=>$info['id'],'cn_name'=>'香港六合彩','diy_name'=>'一分六合彩','description'=>'一分六合彩','remark'=>'无','icon'=>'/sadfa/dasf.png','is_delete'=>0,'create_time'=>timeFormat(time()),'update_time'=>timeFormat(time())],
        ];
        $this->insert('lottery_type', $data);
        //PK10
        $sql = "SELECT * FROM `lottery_type` WHERE code='pk10'";
        $info = $this->fetchRow($sql);
        $data = [
            ['code'=>'30pk10','parent_id'=>$info['id'],'is_sys'=>1,'cn_name'=>'极速赛车','diy_name'=>'极速赛车','description'=>'极速赛车','remark'=>'无','icon'=>'/sadfa/dasf.png','is_delete'=>0,'create_time'=>timeFormat(time()),'update_time'=>timeFormat(time())],
            ['code'=>'yfpk10','parent_id'=>$info['id'],'is_sys'=>1,'cn_name'=>'一分PK拾','diy_name'=>'一分PK拾','description'=>'一分PK拾','remark'=>'无','icon'=>'/sadfa/dasf.png','is_delete'=>0,'create_time'=>timeFormat(time()),'update_time'=>timeFormat(time())],
            ['code'=>'sfpk10','parent_id'=>$info['id'],'is_sys'=>1,'cn_name'=>'三分PK拾','diy_name'=>'三分PK拾','description'=>'三分PK拾','remark'=>'无','icon'=>'/sadfa/dasf.png','is_delete'=>0,'create_time'=>timeFormat(time()),'update_time'=>timeFormat(time())],
            ['code'=>'wfpk10','parent_id'=>$info['id'],'is_sys'=>1,'cn_name'=>'五分PK拾','diy_name'=>'五分PK拾','description'=>'五分PK拾','remark'=>'无','icon'=>'/sadfa/dasf.png','is_delete'=>0,'create_time'=>timeFormat(time()),'update_time'=>timeFormat(time())],
            ['code'=>'shfpk10','parent_id'=>$info['id'],'is_sys'=>1,'cn_name'=>'十分PK拾','diy_name'=>'十分PK拾','description'=>'十分PK拾','remark'=>'无','icon'=>'/sadfa/dasf.png','is_delete'=>0,'create_time'=>timeFormat(time()),'update_time'=>timeFormat(time())],
            ['code'=>'bjpk10','parent_id'=>$info['id'],'cn_name'=>'北京PK拾','diy_name'=>'北京PK拾','description'=>'北京PK拾','remark'=>'无','icon'=>'/sadfa/dasf.png','is_delete'=>0,'create_time'=>timeFormat(time()),'update_time'=>timeFormat(time())],
            ['code'=>'xyft','parent_id'=>$info['id'],'cn_name'=>'幸运飞艇','diy_name'=>'幸运飞艇','description'=>'幸运飞艇','remark'=>'无','icon'=>'/sadfa/dasf.png','is_delete'=>0,'create_time'=>timeFormat(time()),'update_time'=>timeFormat(time())],
        ];
        $this->insert('lottery_type', $data);
        //klsf10分
        $sql = "SELECT * FROM `lottery_type` WHERE code='klsf'";
        $info = $this->fetchRow($sql);
        $data = [
            ['code'=>'30klsf','parent_id'=>$info['id'],'is_sys'=>1,'cn_name'=>'极速快乐10分','diy_name'=>'极速快乐10分','description'=>'极速快乐10分','remark'=>'无','icon'=>'/sadfa/dasf.png','is_delete'=>0,'create_time'=>timeFormat(time()),'update_time'=>timeFormat(time())],
            ['code'=>'yfklsf','parent_id'=>$info['id'],'is_sys'=>1,'cn_name'=>'一分快乐10分','diy_name'=>'一分快乐10分','description'=>'一分快乐10分','remark'=>'无','icon'=>'/sadfa/dasf.png','is_delete'=>0,'create_time'=>timeFormat(time()),'update_time'=>timeFormat(time())],
            ['code'=>'sfklsf','parent_id'=>$info['id'],'is_sys'=>1,'cn_name'=>'三分快乐10分','diy_name'=>'三分快乐10分','description'=>'三分快乐10分','remark'=>'无','icon'=>'/sadfa/dasf.png','is_delete'=>0,'create_time'=>timeFormat(time()),'update_time'=>timeFormat(time())],
            ['code'=>'wfklsf','parent_id'=>$info['id'],'is_sys'=>1,'cn_name'=>'五分快乐10分','diy_name'=>'五分快乐10分','description'=>'五分快乐10分','remark'=>'无','icon'=>'/sadfa/dasf.png','is_delete'=>0,'create_time'=>timeFormat(time()),'update_time'=>timeFormat(time())],
            ['code'=>'shfklsf','parent_id'=>$info['id'],'is_sys'=>1,'cn_name'=>'十分快乐10分','diy_name'=>'十分快乐10分','description'=>'十分快乐10分','remark'=>'无','icon'=>'/sadfa/dasf.png','is_delete'=>0,'create_time'=>timeFormat(time()),'update_time'=>timeFormat(time())],
            ['code'=>'gdklsf','parent_id'=>$info['id'],'cn_name'=>'广东快乐10分','diy_name'=>'广东快乐10分','description'=>'广东快乐10分','remark'=>'无','icon'=>'/sadfa/dasf.png','is_delete'=>0,'create_time'=>timeFormat(time()),'update_time'=>timeFormat(time())],
            ['code'=>'gxklsf','parent_id'=>$info['id'],'cn_name'=>'广西快乐10分','diy_name'=>'广西快乐10分','description'=>'广西快乐10分','remark'=>'无','icon'=>'/sadfa/dasf.png','is_delete'=>0,'create_time'=>timeFormat(time()),'update_time'=>timeFormat(time())],
            ['code'=>'hljklsf','parent_id'=>$info['id'],'cn_name'=>'黑龙江快乐10分','diy_name'=>'黑龙江快乐10分','description'=>'黑龙江快乐10分','remark'=>'无','icon'=>'/sadfa/dasf.png','is_delete'=>0,'create_time'=>timeFormat(time()),'update_time'=>timeFormat(time())],
            ['code'=>'hnklsf','parent_id'=>$info['id'],'cn_name'=>'湖南快乐10分','diy_name'=>'湖南快乐10分','description'=>'湖南快乐10分','remark'=>'无','icon'=>'/sadfa/dasf.png','is_delete'=>0,'create_time'=>timeFormat(time()),'update_time'=>timeFormat(time())],
            ['code'=>'shxklsf','parent_id'=>$info['id'],'cn_name'=>'山西快乐10分','diy_name'=>'山西快乐10分','description'=>'山西快乐10分','remark'=>'无','icon'=>'/sadfa/dasf.png','is_delete'=>0,'create_time'=>timeFormat(time()),'update_time'=>timeFormat(time())],
            ['code'=>'sxklsf','parent_id'=>$info['id'],'cn_name'=>'陕西快乐10分','diy_name'=>'陕西快乐10分','description'=>'陕西快乐10分','remark'=>'无','icon'=>'/sadfa/dasf.png','is_delete'=>0,'create_time'=>timeFormat(time()),'update_time'=>timeFormat(time())],
            ['code'=>'tjklsf','parent_id'=>$info['id'],'cn_name'=>'天津快乐10分','diy_name'=>'天津快乐10分','description'=>'天津快乐10分','remark'=>'无','icon'=>'/sadfa/dasf.png','is_delete'=>0,'create_time'=>timeFormat(time()),'update_time'=>timeFormat(time())],
            ['code'=>'ynklsf','parent_id'=>$info['id'],'cn_name'=>'云南快乐10分','diy_name'=>'云南快乐10分','description'=>'天津快乐10分','remark'=>'无','icon'=>'/sadfa/dasf.png','is_delete'=>0,'create_time'=>timeFormat(time()),'update_time'=>timeFormat(time())],
            ['code'=>'cqklsf','parent_id'=>$info['id'],'cn_name'=>'重庆快乐10分','diy_name'=>'重庆快乐10分','description'=>'重庆快乐10分','remark'=>'无','icon'=>'/sadfa/dasf.png','is_delete'=>0,'create_time'=>timeFormat(time()),'update_time'=>timeFormat(time())],
        ];
        $this->insert('lottery_type', $data);
        //kuai3
        $sql = "SELECT * FROM `lottery_type` WHERE code='kuai3'";
        $info = $this->fetchRow($sql);
        $data = [
            ['code'=>'30ks','parent_id'=>$info['id'],'is_sys'=>1,'cn_name'=>'极速快3','diy_name'=>'极速快3','description'=>'极速快3','remark'=>'无','icon'=>'/sadfa/dasf.png','is_delete'=>0,'create_time'=>timeFormat(time()),'update_time'=>timeFormat(time())],
            ['code'=>'yfks','parent_id'=>$info['id'],'is_sys'=>1,'cn_name'=>'一分快3','diy_name'=>'一分快3','description'=>'一分快3','remark'=>'无','icon'=>'/sadfa/dasf.png','is_delete'=>0,'create_time'=>timeFormat(time()),'update_time'=>timeFormat(time())],
            ['code'=>'sfks','parent_id'=>$info['id'],'is_sys'=>1,'cn_name'=>'三分快3','diy_name'=>'三分快3','description'=>'三分快3','remark'=>'无','icon'=>'/sadfa/dasf.png','is_delete'=>0,'create_time'=>timeFormat(time()),'update_time'=>timeFormat(time())],
            ['code'=>'wfks','parent_id'=>$info['id'],'is_sys'=>1,'cn_name'=>'五分快3','diy_name'=>'五分快3','description'=>'五分快3','remark'=>'无','icon'=>'/sadfa/dasf.png','is_delete'=>0,'create_time'=>timeFormat(time()),'update_time'=>timeFormat(time())],
            ['code'=>'shfks','parent_id'=>$info['id'],'is_sys'=>1,'cn_name'=>'十分快3','diy_name'=>'十分快3','description'=>'十分快3','remark'=>'无','icon'=>'/sadfa/dasf.png','is_delete'=>0,'create_time'=>timeFormat(time()),'update_time'=>timeFormat(time())],
            ['code'=>'fjk3','parent_id'=>$info['id'],'cn_name'=>'福建快3','diy_name'=>'福建快3','description'=>'福建快3','remark'=>'无','icon'=>'/sadfa/dasf.png','is_delete'=>0,'create_time'=>timeFormat(time()),'update_time'=>timeFormat(time())],
            ['code'=>'gsk3','parent_id'=>$info['id'],'cn_name'=>'甘肃快3','diy_name'=>'甘肃快3','description'=>'甘肃快3','remark'=>'无','icon'=>'/sadfa/dasf.png','is_delete'=>0,'create_time'=>timeFormat(time()),'update_time'=>timeFormat(time())],
            ['code'=>'gxk3','parent_id'=>$info['id'],'cn_name'=>'广西快3','diy_name'=>'广西快3','description'=>'广西快3','remark'=>'无','icon'=>'/sadfa/dasf.png','is_delete'=>0,'create_time'=>timeFormat(time()),'update_time'=>timeFormat(time())],
            ['code'=>'gzk3','parent_id'=>$info['id'],'cn_name'=>'贵州快3','diy_name'=>'贵州快3','description'=>'贵州快3','remark'=>'无','icon'=>'/sadfa/dasf.png','is_delete'=>0,'create_time'=>timeFormat(time()),'update_time'=>timeFormat(time())],
            ['code'=>'hbk3','parent_id'=>$info['id'],'cn_name'=>'河北快3','diy_name'=>'河北快3','description'=>'河北快3','remark'=>'无','icon'=>'/sadfa/dasf.png','is_delete'=>0,'create_time'=>timeFormat(time()),'update_time'=>timeFormat(time())],
            ['code'=>'hnk3','parent_id'=>$info['id'],'cn_name'=>'河南快3','diy_name'=>'河南快3','description'=>'河南快3','remark'=>'无','icon'=>'/sadfa/dasf.png','is_delete'=>0,'create_time'=>timeFormat(time()),'update_time'=>timeFormat(time())],
            ['code'=>'hubk3','parent_id'=>$info['id'],'cn_name'=>'湖北快3','diy_name'=>'湖北快3','description'=>'湖北快3','remark'=>'无','icon'=>'/sadfa/dasf.png','is_delete'=>0,'create_time'=>timeFormat(time()),'update_time'=>timeFormat(time())],
            ['code'=>'jlk3','parent_id'=>$info['id'],'cn_name'=>'吉林快3','diy_name'=>'吉林快3','description'=>'吉林快3','remark'=>'无','icon'=>'/sadfa/dasf.png','is_delete'=>0,'create_time'=>timeFormat(time()),'update_time'=>timeFormat(time())],
            ['code'=>'jsk3','parent_id'=>$info['id'],'cn_name'=>'江苏快3','diy_name'=>'江苏快3','description'=>'江苏快3','remark'=>'无','icon'=>'/sadfa/dasf.png','is_delete'=>0,'create_time'=>timeFormat(time()),'update_time'=>timeFormat(time())],
            ['code'=>'jxk3','parent_id'=>$info['id'],'cn_name'=>'江西快3','diy_name'=>'江西快3','description'=>'江西快3','remark'=>'无','icon'=>'/sadfa/dasf.png','is_delete'=>0,'create_time'=>timeFormat(time()),'update_time'=>timeFormat(time())],
            ['code'=>'nmk3','parent_id'=>$info['id'],'cn_name'=>'内蒙古快3','diy_name'=>'内蒙古快3','description'=>'内蒙古快3','remark'=>'无','icon'=>'/sadfa/dasf.png','is_delete'=>0,'create_time'=>timeFormat(time()),'update_time'=>timeFormat(time())],
            ['code'=>'nxk3','parent_id'=>$info['id'],'cn_name'=>'宁夏快3','diy_name'=>'宁夏快3','description'=>'宁夏快3','remark'=>'无','icon'=>'/sadfa/dasf.png','is_delete'=>0,'create_time'=>timeFormat(time()),'update_time'=>timeFormat(time())],
            ['code'=>'qhk3','parent_id'=>$info['id'],'cn_name'=>'青海快3','diy_name'=>'青海快3','description'=>'青海快3','remark'=>'无','icon'=>'/sadfa/dasf.png','is_delete'=>0,'create_time'=>timeFormat(time()),'update_time'=>timeFormat(time())],
            ['code'=>'shk3','parent_id'=>$info['id'],'cn_name'=>'上海快3','diy_name'=>'上海快3','description'=>'上海快3','remark'=>'无','icon'=>'/sadfa/dasf.png','is_delete'=>0,'create_time'=>timeFormat(time()),'update_time'=>timeFormat(time())],
            ['code'=>'xzk3','parent_id'=>$info['id'],'cn_name'=>'西藏快3','diy_name'=>'西藏快3','description'=>'西藏快3','remark'=>'无','icon'=>'/sadfa/dasf.png','is_delete'=>0,'create_time'=>timeFormat(time()),'update_time'=>timeFormat(time())],
            ['code'=>'ahk3','parent_id'=>$info['id'],'cn_name'=>'安徽快3','diy_name'=>'安徽快3','description'=>'安徽快3','remark'=>'无','icon'=>'/sadfa/dasf.png','is_delete'=>0,'create_time'=>timeFormat(time()),'update_time'=>timeFormat(time())],
        ];
        $this->insert('lottery_type', $data);
        //11x5
        $sql = "SELECT * FROM `lottery_type` WHERE code='11x5'";
        $info = $this->fetchRow($sql);
        $data = [
            ['code'=>'3011x5','parent_id'=>$info['id'],'is_sys'=>1,'cn_name'=>'极速11选5','diy_name'=>'极速11选5','description'=>'极速11选5','remark'=>'无','icon'=>'/sadfa/dasf.png','is_delete'=>0,'create_time'=>timeFormat(time()),'update_time'=>timeFormat(time())],
            ['code'=>'yf11x5','parent_id'=>$info['id'],'is_sys'=>1,'cn_name'=>'一分11选5','diy_name'=>'一分11选5','description'=>'一分11选5','remark'=>'无','icon'=>'/sadfa/dasf.png','is_delete'=>0,'create_time'=>timeFormat(time()),'update_time'=>timeFormat(time())],
            ['code'=>'sf11x5','parent_id'=>$info['id'],'is_sys'=>1,'cn_name'=>'三分11选5','diy_name'=>'三分11选5','description'=>'三分11选5','remark'=>'无','icon'=>'/sadfa/dasf.png','is_delete'=>0,'create_time'=>timeFormat(time()),'update_time'=>timeFormat(time())],
            ['code'=>'wf11x5','parent_id'=>$info['id'],'is_sys'=>1,'cn_name'=>'五分11选5','diy_name'=>'五分11选5','description'=>'五分11选5','remark'=>'无','icon'=>'/sadfa/dasf.png','is_delete'=>0,'create_time'=>timeFormat(time()),'update_time'=>timeFormat(time())],
            ['code'=>'shf11x5','parent_id'=>$info['id'],'is_sys'=>1,'cn_name'=>'十分11选5','diy_name'=>'十分11选5','description'=>'十分11选5','remark'=>'无','icon'=>'/sadfa/dasf.png','is_delete'=>0,'create_time'=>timeFormat(time()),'update_time'=>timeFormat(time())],
            ['code'=>'ahsyxw','parent_id'=>$info['id'],'cn_name'=>'安徽11选5','diy_name'=>'安徽11选5','description'=>'安徽11选5','remark'=>'无','icon'=>'/sadfa/dasf.png','is_delete'=>0,'create_time'=>timeFormat(time()),'update_time'=>timeFormat(time())],
            ['code'=>'bjsyxw','parent_id'=>$info['id'],'cn_name'=>'北京11选5','diy_name'=>'北京11选5','description'=>'北京11选5','remark'=>'无','icon'=>'/sadfa/dasf.png','is_delete'=>0,'create_time'=>timeFormat(time()),'update_time'=>timeFormat(time())],
            ['code'=>'fjsyxw','parent_id'=>$info['id'],'cn_name'=>'福建11选5','diy_name'=>'福建11选5','description'=>'福建11选5','remark'=>'无','icon'=>'/sadfa/dasf.png','is_delete'=>0,'create_time'=>timeFormat(time()),'update_time'=>timeFormat(time())],
            ['code'=>'gssyxw','parent_id'=>$info['id'],'cn_name'=>'甘肃11选5','diy_name'=>'甘肃11选5','description'=>'甘肃11选5','remark'=>'无','icon'=>'/sadfa/dasf.png','is_delete'=>0,'create_time'=>timeFormat(time()),'update_time'=>timeFormat(time())],
            ['code'=>'gdsyxw','parent_id'=>$info['id'],'cn_name'=>'广东11选5','diy_name'=>'广东11选5','description'=>'广东11选5','remark'=>'无','icon'=>'/sadfa/dasf.png','is_delete'=>0,'create_time'=>timeFormat(time()),'update_time'=>timeFormat(time())],
            ['code'=>'gxsyxw','parent_id'=>$info['id'],'cn_name'=>'广西11选5','diy_name'=>'广西11选5','description'=>'广西11选5','remark'=>'无','icon'=>'/sadfa/dasf.png','is_delete'=>0,'create_time'=>timeFormat(time()),'update_time'=>timeFormat(time())],
            ['code'=>'gzsyxw','parent_id'=>$info['id'],'cn_name'=>'贵州11选5','diy_name'=>'贵州11选5','description'=>'贵州11选5','remark'=>'无','icon'=>'/sadfa/dasf.png','is_delete'=>0,'create_time'=>timeFormat(time()),'update_time'=>timeFormat(time())],
            ['code'=>'hebsyxw','parent_id'=>$info['id'],'cn_name'=>'河北11选5','diy_name'=>'河北11选5','description'=>'河北11选5','remark'=>'无','icon'=>'/sadfa/dasf.png','is_delete'=>0,'create_time'=>timeFormat(time()),'update_time'=>timeFormat(time())],
            ['code'=>'hljsyxw','parent_id'=>$info['id'],'cn_name'=>'黑龙江11选5','diy_name'=>'黑龙江11选5','description'=>'黑龙江11选5','remark'=>'无','icon'=>'/sadfa/dasf.png','is_delete'=>0,'create_time'=>timeFormat(time()),'update_time'=>timeFormat(time())],
            ['code'=>'hbsyxw','parent_id'=>$info['id'],'cn_name'=>'湖北11选5','diy_name'=>'湖北11选5','description'=>'湖北11选5','remark'=>'无','icon'=>'/sadfa/dasf.png','is_delete'=>0,'create_time'=>timeFormat(time()),'update_time'=>timeFormat(time())],
            ['code'=>'jlsyxw','parent_id'=>$info['id'],'cn_name'=>'吉林11选5','diy_name'=>'吉林11选5','description'=>'吉林11选5','remark'=>'无','icon'=>'/sadfa/dasf.png','is_delete'=>0,'create_time'=>timeFormat(time()),'update_time'=>timeFormat(time())],
            ['code'=>'jssyxw','parent_id'=>$info['id'],'cn_name'=>'江苏11选5','diy_name'=>'江苏11选5','description'=>'江苏11选5','remark'=>'无','icon'=>'/sadfa/dasf.png','is_delete'=>0,'create_time'=>timeFormat(time()),'update_time'=>timeFormat(time())],
            ['code'=>'jxsyxw','parent_id'=>$info['id'],'cn_name'=>'江西11选5','diy_name'=>'江西11选5','description'=>'江西11选5','remark'=>'无','icon'=>'/sadfa/dasf.png','is_delete'=>0,'create_time'=>timeFormat(time()),'update_time'=>timeFormat(time())],
            ['code'=>'lnsyxw','parent_id'=>$info['id'],'cn_name'=>'辽宁11选5','diy_name'=>'辽宁11选5','description'=>'辽宁11选5','remark'=>'无','icon'=>'/sadfa/dasf.png','is_delete'=>0,'create_time'=>timeFormat(time()),'update_time'=>timeFormat(time())],
            ['code'=>'nmgsyxw','parent_id'=>$info['id'],'cn_name'=>'内蒙古11选5','diy_name'=>'内蒙古11选5','description'=>'内蒙古11选5','remark'=>'无','icon'=>'/sadfa/dasf.png','is_delete'=>0,'create_time'=>timeFormat(time()),'update_time'=>timeFormat(time())],
            ['code'=>'nxsyxw','parent_id'=>$info['id'],'cn_name'=>'宁夏11选5','diy_name'=>'宁夏11选5','description'=>'宁夏11选5','remark'=>'无','icon'=>'/sadfa/dasf.png','is_delete'=>0,'create_time'=>timeFormat(time()),'update_time'=>timeFormat(time())],
            ['code'=>'qhsyxw','parent_id'=>$info['id'],'cn_name'=>'青海11选5','diy_name'=>'青海11选5','description'=>'青海11选5','remark'=>'无','icon'=>'/sadfa/dasf.png','is_delete'=>0,'create_time'=>timeFormat(time()),'update_time'=>timeFormat(time())],
            ['code'=>'sdsyxw','parent_id'=>$info['id'],'cn_name'=>'山东11选5','diy_name'=>'山东11选5','description'=>'山东11选5','remark'=>'无','icon'=>'/sadfa/dasf.png','is_delete'=>0,'create_time'=>timeFormat(time()),'update_time'=>timeFormat(time())],
            ['code'=>'shxsyxw','parent_id'=>$info['id'],'cn_name'=>'山西11选5','diy_name'=>'山西11选5','description'=>'山西11选5','remark'=>'无','icon'=>'/sadfa/dasf.png','is_delete'=>0,'create_time'=>timeFormat(time()),'update_time'=>timeFormat(time())],
            ['code'=>'sxsyxw','parent_id'=>$info['id'],'cn_name'=>'陕西11选5','diy_name'=>'陕西11选5','description'=>'陕西11选5','remark'=>'无','icon'=>'/sadfa/dasf.png','is_delete'=>0,'create_time'=>timeFormat(time()),'update_time'=>timeFormat(time())],
            ['code'=>'shsyxw','parent_id'=>$info['id'],'cn_name'=>'上海11选5','diy_name'=>'上海11选5','description'=>'上海11选5','remark'=>'无','icon'=>'/sadfa/dasf.png','is_delete'=>0,'create_time'=>timeFormat(time()),'update_time'=>timeFormat(time())],
            ['code'=>'tjsyxw','parent_id'=>$info['id'],'cn_name'=>'天津11选5','diy_name'=>'天津11选5','description'=>'天津11选5','remark'=>'无','icon'=>'/sadfa/dasf.png','is_delete'=>0,'create_time'=>timeFormat(time()),'update_time'=>timeFormat(time())],
            ['code'=>'xzsyxw','parent_id'=>$info['id'],'cn_name'=>'西藏11选5','diy_name'=>'西藏11选5','description'=>'西藏11选5','remark'=>'无','icon'=>'/sadfa/dasf.png','is_delete'=>0,'create_time'=>timeFormat(time()),'update_time'=>timeFormat(time())],
            ['code'=>'ynsyxw','parent_id'=>$info['id'],'cn_name'=>'云南11选5','diy_name'=>'云南11选5','description'=>'云南11选5','remark'=>'无','icon'=>'/sadfa/dasf.png','is_delete'=>0,'create_time'=>timeFormat(time()),'update_time'=>timeFormat(time())],
            ['code'=>'zjsyxw','parent_id'=>$info['id'],'cn_name'=>'浙江11选5','diy_name'=>'浙江11选5','description'=>'浙江11选5','remark'=>'无','icon'=>'/sadfa/dasf.png','is_delete'=>0,'create_time'=>timeFormat(time()),'update_time'=>timeFormat(time())],
        ];
        $this->insert('lottery_type', $data);
        //快乐8
        $sql = "SELECT * FROM `lottery_type` WHERE code='klb'";
        $info = $this->fetchRow($sql);
        $data = [
            ['code'=>'30klb','parent_id'=>$info['id'],'is_sys'=>1,'cn_name'=>'极速快乐8','diy_name'=>'极速快乐8','description'=>'极速快乐8','remark'=>'无','icon'=>'/sadfa/dasf.png','is_delete'=>0,'create_time'=>timeFormat(time()),'update_time'=>timeFormat(time())],
            ['code'=>'yfklb','parent_id'=>$info['id'],'is_sys'=>1,'cn_name'=>'一分快乐8','diy_name'=>'一分快乐8','description'=>'一分快乐8','remark'=>'无','icon'=>'/sadfa/dasf.png','is_delete'=>0,'create_time'=>timeFormat(time()),'update_time'=>timeFormat(time())],
            ['code'=>'sfklb','parent_id'=>$info['id'],'is_sys'=>1,'cn_name'=>'三分快乐8','diy_name'=>'三分快乐8','description'=>'三分快乐8','remark'=>'无','icon'=>'/sadfa/dasf.png','is_delete'=>0,'create_time'=>timeFormat(time()),'update_time'=>timeFormat(time())],
            ['code'=>'wfklb','parent_id'=>$info['id'],'is_sys'=>1,'cn_name'=>'五分快乐8','diy_name'=>'五分快乐8','description'=>'五分快乐8','remark'=>'无','icon'=>'/sadfa/dasf.png','is_delete'=>0,'create_time'=>timeFormat(time()),'update_time'=>timeFormat(time())],
            ['code'=>'shfklb','parent_id'=>$info['id'],'is_sys'=>1,'cn_name'=>'十分快乐8','diy_name'=>'十分快乐8','description'=>'十分快乐8','remark'=>'无','icon'=>'/sadfa/dasf.png','is_delete'=>0,'create_time'=>timeFormat(time()),'update_time'=>timeFormat(time())],
            ['code'=>'bjklb','parent_id'=>$info['id'],'cn_name'=>'北京快乐8','diy_name'=>'北京快乐8','description'=>'北京快乐8','remark'=>'无','icon'=>'/sadfa/dasf.png','is_delete'=>0,'create_time'=>timeFormat(time()),'update_time'=>timeFormat(time())],
            ['code'=>'jndklb','parent_id'=>$info['id'],'cn_name'=>'加拿大快乐8','diy_name'=>'加拿大快乐8','description'=>'加拿大快乐8','remark'=>'无','icon'=>'/sadfa/dasf.png','is_delete'=>0,'create_time'=>timeFormat(time()),'update_time'=>timeFormat(time())],
            ['code'=>'jpzklb','parent_id'=>$info['id'],'cn_name'=>'柬埔寨快乐8','diy_name'=>'柬埔寨快乐8','description'=>'柬埔寨快乐8','remark'=>'无','icon'=>'/sadfa/dasf.png','is_delete'=>0,'create_time'=>timeFormat(time()),'update_time'=>timeFormat(time())],
        ];
        $this->insert('lottery_type', $data);
        //xy28
        $sql = "SELECT * FROM `lottery_type` WHERE code='xy28'";
        $info = $this->fetchRow($sql);
        $data = [
            ['code'=>'30xy28','parent_id'=>$info['id'],'is_sys'=>1,'cn_name'=>'极速幸运28','diy_name'=>'极速幸运28','description'=>'极速幸运28','remark'=>'无','icon'=>'/sadfa/dasf.png','is_delete'=>0,'create_time'=>timeFormat(time()),'update_time'=>timeFormat(time())],
            ['code'=>'yfxy28','parent_id'=>$info['id'],'is_sys'=>1,'cn_name'=>'一分幸运28','diy_name'=>'一分幸运28','description'=>'一分幸运28','remark'=>'无','icon'=>'/sadfa/dasf.png','is_delete'=>0,'create_time'=>timeFormat(time()),'update_time'=>timeFormat(time())],
            ['code'=>'sfxy28','parent_id'=>$info['id'],'is_sys'=>1,'cn_name'=>'三分幸运28','diy_name'=>'三分幸运28','description'=>'三分幸运28','remark'=>'无','icon'=>'/sadfa/dasf.png','is_delete'=>0,'create_time'=>timeFormat(time()),'update_time'=>timeFormat(time())],
            ['code'=>'wfxy28','parent_id'=>$info['id'],'is_sys'=>1,'cn_name'=>'五分幸运28','diy_name'=>'五分幸运28','description'=>'五分幸运28','remark'=>'无','icon'=>'/sadfa/dasf.png','is_delete'=>0,'create_time'=>timeFormat(time()),'update_time'=>timeFormat(time())],
            ['code'=>'shfxy28','parent_id'=>$info['id'],'is_sys'=>1,'cn_name'=>'十分幸运28','diy_name'=>'十分幸运28','description'=>'十分幸运28','remark'=>'无','icon'=>'/sadfa/dasf.png','is_delete'=>0,'create_time'=>timeFormat(time()),'update_time'=>timeFormat(time())],
            ['code'=>'bjxy28','parent_id'=>$info['id'],'cn_name'=>'北京幸运28','diy_name'=>'北京幸运28','description'=>'北京幸运28','remark'=>'无','icon'=>'/sadfa/dasf.png','is_delete'=>0,'create_time'=>timeFormat(time()),'update_time'=>timeFormat(time())],
        ];
        $this->insert('lottery_type', $data);
    }
}
