<?php

use think\migration\Migrator;
use think\migration\db\Column;

class Opencode extends Migrator
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('opencode',array('engine'=>'InnoDB','comment'=>'开奖结果'));
        $table
            ->addColumn('lottery_type_id', 'integer',array('limit' => 6,'default'=>0,'comment'=>'彩票分类ID'))
            ->addColumn('issue', 'biginteger',array('limit' =>12,'default'=>'0','comment'=>'期号'))
            ->addColumn('opencode', 'string',array('limit' => 40,'default'=>'0','comment'=>'开奖结果'))
            ->addColumn('create_time', 'datetime',array('comment'=>'创建时间'))
            ->addColumn('update_time', 'datetime',array('comment'=>'更新时间'))
            ->addIndex(array('lottery_type_id','issue'), array('unique' => true))//联合索引，唯一
            ->create();
    }
}
