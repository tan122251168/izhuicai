<?php

use think\migration\Migrator;
use think\migration\db\Column;

class Recharge extends Migrator
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('recharge',array('engine'=>'InnoDB','comment'=>'充值订单'));
        $table
            ->addColumn('user_id', 'integer',array('limit' => 11,'default'=>'0','comment'=>'用户ID'))
            ->addColumn('balance', 'decimal',array('precision' => 12,'scale'=>2,'default'=>0,'comment'=>'金额'))
            ->addColumn('ordernum', 'string',array('limit' =>22,'default'=>'0','comment'=>'订单号'))
            ->addColumn('gate_id', 'integer',array('limit' => 11,'default'=>0,'comment'=>'支付网关ID'))
            ->addColumn('is_pass', 'boolean',array('limit' => 1,'default'=>0,'comment'=>'未审核，1已通过，2已经拒绝，3已经忽略'))
            ->addColumn('remark', 'string',array('limit' => 32,'default'=>'0','comment'=>'备注'))
            ->addColumn('create_time', 'datetime',array('comment'=>'创建时间'))
            ->addColumn('update_time', 'datetime',array('comment'=>'更新时间'))
            ->addIndex(array('user_id','ordernum'))
            ->create();
    }
}
