<?php

use think\migration\Migrator;
use think\migration\db\Column;

class Gate extends Migrator
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('gate',array('engine'=>'InnoDB','comment'=>'支付网关'));
        $table
            ->addColumn('gtype', 'integer',array('limit' =>2,'default'=>0,'comment'=>'类型：1二维码2银行卡转账3在线支付'))
            ->addColumn('gname', 'string',array('limit' => 32,'default'=>'0','comment'=>'网关名字'))
            ->addColumn('is_delete', 'boolean',array('limit' => 1,'default'=>0,'comment'=>'删除状态，1已删除'))
            ->addColumn('remark', 'string',array('limit' => 32,'default'=>'0','comment'=>'备注'))
            ->addColumn('create_time', 'datetime',array('comment'=>'创建时间'))
            ->addColumn('update_time', 'datetime',array('comment'=>'更新时间'))
            ->create();
    }
}
