<?php

use think\migration\Migrator;
use think\migration\db\Column;

class GateQrcode extends Migrator
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('gate_qrcode',array('engine'=>'InnoDB','comment'=>'支付网关二维码'));
        $table
            ->addColumn('gate_id', 'integer',array('limit' =>11,'default'=>0,'comment'=>'网关id'))
            ->addColumn('qrcode_url', 'string',array('limit' =>200,'default'=>'0','comment'=>'收款码网址'))
            ->addColumn('create_time', 'datetime',array('comment'=>'创建时间'))
            ->addColumn('update_time', 'datetime',array('comment'=>'更新时间'))
            ->create();
    }
}
