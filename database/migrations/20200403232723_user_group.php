<?php

use think\migration\Migrator;
use think\migration\db\Column;

class UserGroup extends Migrator
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('user_group',array('engine'=>'InnoDB','comment'=>'用户组'));
        $table
            ->addColumn('group_name', 'string',array('limit' => 32,'default'=>'0','comment'=>'分组名字'))
            ->addColumn('remark', 'string',array('limit' => 32,'default'=>'0','comment'=>'备注'))
            ->addColumn('sign_in', 'integer',array('limit' =>1,'default'=>0,'comment'=>'是否可签到：1开启，0关闭'))
            ->addColumn('sign_in_reward', 'decimal',array('precision' => 12,'scale'=>2,'default'=>0,'comment'=>'签到赠送金额'))
            ->addColumn('withdraw_times', 'integer',array('limit' =>3,'default'=>0,'comment'=>'每日可提现次数'))
            ->addColumn('withdraw_min', 'decimal',array('precision' => 12,'scale'=>2,'default'=>0,'comment'=>'单笔最低可提现金额'))
            ->addColumn('withdraw_max', 'decimal',array('precision' => 12,'scale'=>2,'default'=>0,'comment'=>'单笔最高可提现金额'))
            ->addColumn('total_recharge', 'decimal',array('precision' => 12,'scale'=>2,'default'=>0,'comment'=>'所需充值量'))
            ->addColumn('create_time', 'datetime',array('comment'=>'创建时间'))
            ->addColumn('update_time', 'datetime',array('comment'=>'更新时间'))
            ->create();
    }
}
